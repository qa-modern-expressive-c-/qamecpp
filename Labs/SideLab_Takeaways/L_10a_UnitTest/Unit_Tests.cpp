#include <catch.hpp>
#include "language.h"

using namespace std;

using namespace language;

// Lab 10 Unit Tests
TEST_CASE("language()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.language() == string("Assembly"));
}

TEST_CASE("skill()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.skill() == int(basic));
}

TEST_CASE("skill_str()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.skill_str(basic) == string("Basic"));
}

TEST_CASE("upgrade()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	l1.upgrade();
	CHECK(l1.skill() == int(competant));
}

TEST_CASE("valid()", "[Language Tests]") {
	auto l1 = Language{};
	CHECK(l1.isValid() == false);
	auto l2 = Language{ "Assembly", basic };
	CHECK(l2.isValid() == true);
}