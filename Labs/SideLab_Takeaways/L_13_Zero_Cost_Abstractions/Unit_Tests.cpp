#include <catch.hpp>
#include "language.h"
#include <array>

using namespace std;

using namespace language;

TEST_CASE("language()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.language() == string("Assembly"));
}

TEST_CASE("skill()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.skill() == int(basic));
}

TEST_CASE("skill_str()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.skill_str(basic) == string("Basic"));
}

TEST_CASE("upgrade()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	l1.upgrade();
	CHECK(l1.skill() == int(competant));
}

TEST_CASE("valid()", "[Language Tests]") {
	auto l1 = Language{};
	CHECK(l1.isValid() == false);
	auto l2 = Language{ "Assembly", basic };
	CHECK(l2.isValid() == true);
}

TEST_CASE("Iterators", "[Language Tests]") {
	auto l1 = Language{ "Assembly", none };
	auto l2 = Language{ "Visual Basic", little };
	auto l3 = Language{ "C", basic };
	auto l4 = Language{ "C++", competant };
	auto l5 = Language{ "Java", skilled };

	auto  my_languages = array<Language, 10>{l1, l2, l3, l4, l5};
	auto checkCount = 0;

	for (auto it = my_languages.begin(); it != my_languages.end(); ++it) {
		if (it->isValid()) {
			auto originalSkill = it->skill();
			CHECK(originalSkill == distance(my_languages.begin(), it));
			it->upgrade();
			if (originalSkill < skilled) {
				CHECK(it->skill() == originalSkill + 1);
				++checkCount;
			}
		}
	}
	CHECK(checkCount == 4);
}

// Lab 12 Pointers

TEST_CASE("Pointer", "[Language Tests]") {
	auto lnull = Language{};
	auto l1 = Language{ "Assembly", none };
	auto l2 = Language{ "Visual Basic", little };
	auto l3 = Language{ "C", basic };
	auto l4 = Language{ "C++", competant };
	auto l6 = Language{ "Java", skilled };

	auto  my_languages = array<Language, 10>{lnull, l1, l2, lnull, l3, l4, lnull, l6, lnull};
	auto checkCount = 0;
	const Language *  prevLanguage = nullptr;
	for (unsigned int i = 0; i < my_languages.size(); ++i) {
		 Language * const lptr = &my_languages[i];
		if (lptr->isValid()) {
			if (prevLanguage) {
				CHECK(lptr->skill() == prevLanguage->skill() + 1);
				++checkCount;
			}
			prevLanguage = lptr;
		}
	}
	CHECK(checkCount == 4);
}

// Lab 13 Zero_Cost_Abstractions

TEST_CASE("Enums", "[Language Tests]") {
	auto lt1 = Language_Type{ "Python", garbageCollection };
	auto lt2 = Language_Type{ "C", braces | stackBasedObjects | pointers };
	auto lt3 = Language_Type{ "C++", braces | stackBasedObjects | pointers | objectCopying };
	auto lt4 = Language_Type{ "C#", braces | stackBasedObjects | pointers | objectCopying | garbageCollection };
	auto lt5 = Language_Type{ "Java", braces | garbageCollection };

	auto language_types = array<Language_Type, 10>{lt1, lt2, lt3, lt4, lt5};
	
	auto bracesCount = 0;
	auto stackBasedObjectsCount = 0;
	auto pointersCount = 0;
	auto objectCopyingCount = 0;
	auto garbageCollectionCount = 0;
	for (auto & lang : language_types) {
		if (lang.distinctives & braces) ++bracesCount;
		if (lang.distinctives & stackBasedObjects) ++stackBasedObjectsCount;
		if (lang.distinctives & pointers) ++pointersCount;
		if (lang.distinctives & objectCopying) ++objectCopyingCount;
		if (lang.distinctives & garbageCollection) ++garbageCollectionCount;
	}
	CHECK(bracesCount == 4);
	CHECK(stackBasedObjectsCount == 3);
	CHECK(pointersCount == 3);
	CHECK(objectCopyingCount == 2);
	CHECK(garbageCollectionCount == 3);
}
