// L_02_Language_Overview.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <array>

using namespace std;

namespace language {
	// type definitions
	enum Skill { none, little, basic, competant, skilled };
	struct Language { 
		const string language; 
		Skill skill;
		void upgrade();
		void print() const;
	};
	using Boast_t = unsigned int;

	auto limit_boasting(Boast_t exageration)->Boast_t;
	auto show_languages(const Language & lang, Boast_t boast = 1)->string;
}

namespace lg = language;


int main()
{
	// Entity Definitions
	auto l1 = lg::Language{ "Assembly", lg::basic };
	auto l2 = lg::Language{ "Visual Basic", lg::competant };
	auto l3 = lg::Language{ "C", lg::skilled };
	auto l4 = lg::Language{ "C++", lg::little };

	auto  my_languages = array<lg::Language,10>{l1,l2,l3,l4};

	auto & l1_language = l1.language;
	//l1_language = "Java";
	
	auto skills_Boast_t = lg::Boast_t{ 2 };

	cout << "\nOriginal Skills\n" << endl;
	for (const auto & lang : my_languages) {
		show_languages(lang);
		//lang.upgrade();
	}
	
	cout << "\nBoasted Skills\n" << endl;
	for (const auto & lang : my_languages) {
		show_languages(lang, skills_Boast_t);
	}
	
	cout << "\nUpgraded Skills\n" << endl;
	for (auto & lang : my_languages) {
		if (lang.language.empty()) continue;
		lang.upgrade();
		lang.print();
	}
}
namespace language {

	auto limit_boasting(Boast_t exageration) -> Boast_t {
		if (exageration > skilled) exageration = skilled;
		return exageration;
	}

	void show_languages(const Language & lang, Boast_t boast) {
		if (!lang.language.empty()) {
			cout << lang.language << " Skill:" << limit_boasting(lang.skill * boast) << endl;
		}
	}

	void Language::upgrade() {
		if (skill < skilled) {
			skill = Skill(skill + 1);
		}
	}

	void Language::print() const {
		cout << language << " Skill:" << skill << endl;
	}
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
