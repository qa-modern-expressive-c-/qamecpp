// L_02_Language_Overview.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <array>
#include <sstream>
#include <iomanip>

using namespace std;

namespace language {
	// type definitions
	enum Skill { none, little, basic, competant, skilled };
	class Language { 
	public:
		Language(const char* language, Skill skill) : _language{ language }, _skill{ skill } {}
		Language() = default;
		// Query Functions
		auto print() const -> string ;
		auto language() const -> const string & { return _language; } // immutable reference to prevent copying of the string
		auto skill() const { return _skill; }				// return primitive by copy is efficient
		auto isValid() const { return !_language.empty(); }
		// Modifier function
		void upgrade();
	private:
		const string _language; 
		Skill _skill;
	};
	using Boast_t = unsigned int;

	auto limit_boasting(Boast_t exageration) ->Boast_t;
	auto show_languages(const Language & lang, Boast_t boast = 1) -> string;
}

namespace lg = language;

int main()
{
	// Entity Definitions
	auto l1 = lg::Language{ "Assembly", lg::basic };
	auto l2 = lg::Language{ "Visual Basic", lg::competant };
	auto l3 = lg::Language{ "C", lg::skilled };
	auto l4 = lg::Language{ "C++", lg::little };

	auto  my_languages = array<lg::Language,10>{l1,l2,l3,l4};

	//auto & l1_language = l1.language;
	//l1_language = "Java";
	
	auto skills_boost = lg::Boast_t{ 2 };

	cout << "\nOriginal Skills\n" << endl;
	for (const auto & lang : my_languages) {
		cout << show_languages(lang);
		//lang.upgrade();
	}
	
	cout << "\nBoasted Skills\n" << endl;
	for (const auto & lang : my_languages) {
		cout << show_languages(lang, skills_boost);
	}
	
	cout << "\nUpgraded Skills\n" << endl;
	for (auto & lang : my_languages) {
		if (!lang.isValid()) continue;
		lang.upgrade();
		cout << lang.print();
	}
}

namespace language {
	auto limit_boasting(Boast_t exageration) -> Boast_t {
		if (exageration > skilled) exageration = skilled;
		return exageration;
	}

	string show_languages(const Language & lang, Boast_t boast) {
		auto outStream = stringstream{};
		if (lang.isValid()) {
			outStream << left << setw(20) << lang.language() << " Skill:" << right << setw(2) << setfill('0') << limit_boasting(lang.skill() * boast) << endl;
		}
		return outStream.str();
	}

	void Language::upgrade() {
		if (_skill < skilled) {
			_skill = Skill(_skill + 1);
		}
	}

	string Language::print() const {
		auto outStream = stringstream{};
		outStream << left << setw(20) << language() << " Skill: " << right << setw(2) << setfill('0') << skill() << endl;
		return outStream.str();
	}
}


// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
