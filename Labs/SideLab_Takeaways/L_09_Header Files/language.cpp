#include "language.h"
#include <sstream>
#include <iomanip>
#include <assert.h>

using namespace std;

namespace language {

	auto limit_boasting(Boast_t exageration) -> Boast_t {
		if (exageration > skilled) exageration = skilled;
		return exageration;
	}

	string show_languages(const Language & lang, Boast_t boast) {
		auto outStream = stringstream{};
		if (lang.isValid()) {
			auto skill_wish = limit_boasting(lang.skill() * boast);
			outStream << left << setw(20) << lang.language()
				<< " Skill:" << right << setw(2) << setfill('0') << skill_wish
				<< " " << lang.skill_str(skill_wish) << endl;
		}
		return outStream.str();
	}

	void Language::upgrade() {
		if (_skill < skilled) {
			_skill = Skill(_skill + 1);
		}
	}

	string Language::print() const {
		auto outStream = stringstream{};
		outStream << left << setw(20) << language() << " Skill: " << right << setw(2) << setfill('0') << skill() << " " << skill_str(skill()) << endl;
		return outStream.str();
	}

	auto Language::skill_str(int skill) const -> string {
		switch (skill) {
		case none: return "None";
		case little: return "Little";
		case basic: return "Basic";
		case competant: return "Competant";
		case skilled: return "Skilled";
		default: assert(skill <= skilled);
			return "";
		}
	}
}