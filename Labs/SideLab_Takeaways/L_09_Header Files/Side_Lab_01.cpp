// L_02_Language_Overview.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "language.h"
#include <iostream>
#include <array>


using namespace std;

namespace lg = language;

int main()
{
	// Entity Definitions
	auto l1 = lg::Language{ "Assembly", lg::basic };
	auto l2 = lg::Language{ "Visual Basic", lg::competant };
	auto l3 = lg::Language{ "C", lg::skilled };
	auto l4 = lg::Language{ "C++", lg::little };

	auto  my_languages = array<lg::Language,10>{l1,l2,l3,l4};

	//auto & l1_language = l1.language;
	//l1_language = "Java";
	
	auto skills_boost = lg::Boast_t{ 2 };

	cout << "\nOriginal Skills\n" << endl;
	for (const auto & lang : my_languages) {
		cout << show_languages(lang);
		//lang.upgrade();
	}
	
	cout << "\nBoasted Skills\n" << endl;
	for (const auto & lang : my_languages) {
		cout << show_languages(lang, skills_boost);
	}
	
	cout << "\nUpgraded Skills\n" << endl;
	for (auto & lang : my_languages) {
		if (!lang.isValid()) continue;
		lang.upgrade();
		cout << lang.print();
	}
}



// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
