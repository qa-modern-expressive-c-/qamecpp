#pragma once
#include <string>

namespace language {
	// Globals
	extern size_t language_col_width;
	// type definitions
	enum Skill { none, little, basic, competant, skilled };
	class Language {
	public:
		Language(const char* language, Skill skill) : _language{ language }, _skill{ skill } {}
		Language() = default;
		// Query Functions
		auto print() const-> std::string;
		auto language() const -> const std::string & { return _language; } // immutable reference to prevent copying of the string
		auto skill() const { return _skill; }				// return primitive by copy is efficient
		auto skill_str(int skill) const-> std::string;
		auto isValid() const { return !_language.empty(); }
		// Modifier function
		void upgrade();
	private:
		const std::string _language;
		Skill _skill;
	};
	using Boast_t = unsigned int;

	inline auto limit_boasting(language::Boast_t exageration) { return exageration > language::skilled ? language::skilled : exageration; }
	auto show_languages(const language::Language & lang, language::Boast_t boast = 1)->std::string;

	enum Language_Distinctive{ braces = 1, stackBasedObjects = 2, pointers = 4, objectCopying = 8, garbageCollection = 16 };
	struct Language_Type {
		const std::string language;
		int distinctives;
	};
}