#include "language.h"
#include <sstream>
#include <iomanip>
#include <assert.h>

using namespace std;

namespace language {

	string show_languages(const Language & lang, Boast_t boast) {
		auto outStream = stringstream{};
		if (lang.isValid()) {
			auto skill_wish = limit_boasting(lang.skill() * boast);
			outStream << left << setw(language_col_width) << lang.language()
				<< " Skill:" << right << setw(2) << setfill('0') << skill_wish
				<< " " << lang.skill_str(skill_wish) << endl;
		}
		return outStream.str();
	}

	size_t language_col_width = 20;

	void Language::upgrade() {
		if (_skill < skilled) {
			_skill = Skill(_skill + 1);
		}
	}

	string Language::print() const {
		auto outStream = stringstream{};
		outStream << left << setw(language_col_width) << language() << " Skill: " << right << setw(2) << setfill('0') << skill() << " " << skill_str(skill()) << endl;
		return outStream.str();
	}

	auto Language::skill_str(int skill) const -> string {
		switch (skill) {
		case none: return "None";
		case little: return "Little";
		case basic: return "Basic";
		case competant: return "Competant";
		case skilled: return "Skilled";
		default: assert(skill <= skilled);
			return "";
		}
	}
}