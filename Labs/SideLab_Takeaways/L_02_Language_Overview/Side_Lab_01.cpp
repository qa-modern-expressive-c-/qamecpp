// L_02_Language_Overview.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
using namespace std;

int main()
{
	cout << "Hello World!" << endl;
	// type definitions
	enum Skill {none, little, basic, competant, skilled};
	struct Language { string language; Skill skill; };

	// Entity Definitions
	Language l1 = { "Assembly", basic };
	Language l2 = { "Visual Basic", competant };
	Language l3 = { "C", skilled };
	int skills_boost = 2;

	cout << l1.language << " Skill:" << l1.skill << endl;
	cout << l2.language << " Skill:" << l2.skill << endl;
	cout << l3.language << " Skill:" << l3.skill << endl;

	cout << endl << "Exagerated Skills!" << endl;
	cout << l1.language << " Skill:" << l1.skill * skills_boost << endl;
	cout << l2.language << " Skill:" << l2.skill * skills_boost << endl;
	cout << l3.language << " Skill:" << l3.skill * skills_boost << endl;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
