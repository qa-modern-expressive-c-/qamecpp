#include <catch.hpp>
#include "language.h"
#include <array>

using namespace std;

using namespace language;

// Lab 10 Unit Tests

TEST_CASE("language()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.language() == string("Assembly"));
}

TEST_CASE("skill()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.skill() == int(basic));
}

TEST_CASE("skill_str()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	CHECK(l1.skill_str(basic) == string("Basic"));
}

TEST_CASE("upgrade()", "[Language Tests]") {
	auto l1 = Language{ "Assembly", basic };
	l1.upgrade();
	CHECK(l1.skill() == int(competant));
}

TEST_CASE("valid()", "[Language Tests]") {
	auto l1 = Language{};
	CHECK(l1.isValid() == false);
	auto l2 = Language{ "Assembly", basic };
	CHECK(l2.isValid() == true);
}

// Lab 11 Iterators

TEST_CASE("Iterators", "[Language Tests]") {
	auto l1 = Language{ "Assembly", none };
	auto l2 = Language{ "Visual Basic", little };
	auto l3 = Language{ "C", basic };
	auto l4 = Language{ "C++", competant };
	auto l5 = Language{ "Java", skilled };

	auto  my_languages = array<Language, 10>{l1, l2, l3, l4, l5};
	auto checkCount = 0;

	for (auto it = my_languages.begin(); it != my_languages.end(); ++it) {
		if (it->isValid()) {
			auto originalSkill = it->skill();
			CHECK(originalSkill == distance(my_languages.begin(), it));
			it->upgrade();
			if (originalSkill < skilled) {
				CHECK(it->skill() == originalSkill + 1);
				++checkCount;
			}
		}
	}
	CHECK(checkCount == 4);
}

// Lab 12 Pointers

TEST_CASE("Pointer", "[Language Tests]") {
	auto lnull = Language{};
	auto l1 = Language{ "Assembly", none };
	auto l2 = Language{ "Visual Basic", little };
	auto l3 = Language{ "C", basic };
	auto l4 = Language{ "C++", competant };
	auto l6 = Language{ "Java", skilled };

	auto  my_languages = array<Language, 10>{lnull, l1, l2, lnull, l3, l4, lnull, l6, lnull};
	auto checkCount = 0;
	const Language *  prevLanguage = nullptr;
	for (unsigned int i = 0; i < my_languages.size(); ++i) {
		const Language * const lptr = &my_languages[i];
		if (lptr->isValid()) {
			if (prevLanguage) {
				CHECK(lptr->skill() == prevLanguage->skill() + 1);
				++checkCount;
			}
			prevLanguage = lptr;
		}
	}
	CHECK(checkCount == 4);
}
