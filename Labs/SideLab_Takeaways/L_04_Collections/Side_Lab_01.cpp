// L_02_Language_Overview.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <array>

using namespace std;

namespace language {
	// type definitions
	enum Skill { none, little, basic, competant, skilled };
	struct Language { const string language; Skill skill; };
}

namespace lg = language;

auto limit_boasting(int exageration) -> int;

int main()
{
	// Entity Definitions
	auto l1 = lg::Language{ "Assembly", lg::basic };
	auto l2 = lg::Language{ "Visual Basic", lg::competant };
	auto l3 = lg::Language{ "C", lg::skilled };
	auto l4 = lg::Language{ "C++", lg::little };

	auto  my_languages = array<lg::Language,10>{l1,l2,l3,l4};

	auto & l1_language = l1.language;
	//l1_language = "Java";
	
	auto skills_boost = int{ 2 };

	for (const auto & lang : my_languages) {
		cout << lang.language << " Skill:" << limit_boasting(lang.skill * skills_boost) << endl;
	}
}

auto limit_boasting(int exageration) -> int {
	if (exageration > lg::skilled) exageration = lg::skilled;
	return exageration;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
