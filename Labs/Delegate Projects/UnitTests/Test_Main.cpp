#include "pch.h"
#define CATCH_CONFIG_RUNNER
#include <catch.hpp>

int main()
{
	Catch::Session session; // There must be exactly once instance

	return session.run();
}