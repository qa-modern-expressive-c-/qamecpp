#include "pch.h"
#include <catch.hpp>
#include <string>
#include "string_utilities.h"

using namespace std;
using namespace string_utilities;

// Lab 10 Unit Tests

TEST_CASE("Each Column Present", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d" };
	auto tokenized = splitString(sourceString,',',4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
}
