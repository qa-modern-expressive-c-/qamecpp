#include "pch.h"
#include"fileHandler.h"
#include <sstream>

using namespace std;
namespace fs = std::filesystem;

FileHandler::FileHandler(const fs::path & rootPath, const fs::path & extention)
	: _rootPath{ rootPath }
	, _extension{ extention }
{}

void FileHandler::setPath(std::string path) {
	_rootPath = path;
}

void FileHandler::getFilePaths() {
	_filePaths = getFilePaths(_rootPath, _extension);
}

auto FileHandler::getFilePaths(const fs::path & pathToShow, const fs::path & extension) -> vector<fs::path> {
	return { pathToShow, extension };
}

auto FileHandler::getLinesFromFileAt(size_t _fileSelectionIndex) const -> vector<string> {
	return { "K,11,b,a,E01,declaration,auto,notes" };
}

void FileHandler::writeFile(const std::filesystem::path & fileName, const std::vector<std::string> & lines) {
}