#pragma once
// STL Headers...
//#include <filesystem>
#include <filesystem>
#include <vector>
#include <string>

class FileHandler {
public:
	FileHandler(const std::filesystem::path & rootPath, const std::filesystem::path & extension);

	// Queries
	auto const & filePaths() const { return _filePaths; }
	auto getLinesFromFileAt(size_t fileSelectionIndex) const->std::vector<std::string>;

	// Modifiers
	void setPath(std::string);
	void getFilePaths();
	auto getFilePaths(const std::filesystem::path & pathToShow, const std::filesystem::path & ext)->std::vector<std::filesystem::path>;
	void writeFile(const std::filesystem::path & fileName, const std::vector<std::string> & lines);

private:
	std::vector<std::filesystem::path> _filePaths;
	std::filesystem::path _rootPath;
	std::filesystem::path _extension;
};

