#pragma once
#include"CPP_Feature.h"
#include <string>
#include <vector>

namespace cpp_features {
	/////////////// Feature Collection Class /////////////

	class CPP_Feature_Collection : public std::vector<CPP_Language_Feature> {
	public:
		// bring the hidden baseclass constructors into scope
		using std::vector<CPP_Language_Feature>::vector;
		static auto optionRangeFor(TSV_Fields) -> const Val_Option_Range &;
		// Queries
		auto keyRange(TSV_Fields) const->std::vector<std::string>;
		auto valueRange(TSV_Fields) const->std::vector<std::string>;
		auto rangePos(TSV_Fields, std::string fieldKey) const->std::size_t;
		auto keyAt(TSV_Fields, std::size_t pos) const->std::string;
		auto featureAt(size_t pos) const -> std::vector<std::string> { return at(pos).asVector(); }

		// Modifiers
		auto findFeature(std::vector<std::string> feature) -> std::vector<CPP_Language_Feature>::iterator;
		void addFeature(std::vector<std::string> arg);
		void editFeature(size_t pos, std::vector<std::string> edit);
		void eraseFeature(size_t pos);
	private:
	};
}