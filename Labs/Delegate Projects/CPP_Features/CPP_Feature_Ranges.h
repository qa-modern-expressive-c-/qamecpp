#pragma once
#include "string_utilities.h"
#include <string>
#include <deque>
#include <initializer_list>

namespace cpp_features {

	class Val_Option_Range : public std::deque<std::string> {
	public:
		// bring the hidden baseclass constructors into scope
		using std::deque<std::string>::deque;
		// Queries
		auto keyRange() const->std::vector<std::string> ;
		auto valueRange() const -> std::vector<std::string> { return keyRange(); }
		auto keyAt(size_t pos) const->std::string;
		auto rangePos(std::string fieldKey) const -> std::size_t;
		auto keyMatches(std::string item) const->std::string;
	};
}