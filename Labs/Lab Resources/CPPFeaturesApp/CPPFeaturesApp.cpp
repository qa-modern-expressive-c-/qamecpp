#include "pch.h"
#include "gui.h"	// user-defined gui definition
// nana GUI Creation Headers...
#include <nana/gui/wvl.hpp> // for nana::exec()
// STL Headers...
#include <filesystem>

namespace fs = std::filesystem;

int main()
{
	GUI gui{ fs::current_path().parent_path().parent_path()/="Delegate Projects" };
	gui.show();

	nana::exec();
}