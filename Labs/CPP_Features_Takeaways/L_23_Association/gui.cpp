#include "pch.h"
#include "gui.h"
#include "gui_column_sort.h"
#include "string_utilities.h"
#include "CPP_Global_Constants.h"
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;
using namespace nana;
using namespace string_utilities;
using namespace cpp_features;
using namespace user_info;

namespace fs = std::filesystem;

enum class GUI::Change_Behaviour::Transitions : int {
	initialise, featureSelected, gotSearchTerm, featureEdited, newFeatureRequested, editFeatureCompleted, userInfoSelected, userInfoEdited, userInfoCompleted
};

GUI::GUI(const fs::path appFolder) 
	: nana::form{ nana::API::make_center(1000, 500) }
	, _folder_lbl{*this,"Folder-Path"}
	, _folderPath{*this, appFolder.string() }
	, _fileList{ *this }
	, _userList{ *this }
	, _loadFileCmd{ *this, "Load File"}
	, _tidyFileCmd{*this, "Tidy File"}
	, _saveAsCmd{*this, "Save File"}
	, _saveEditCmd{*this, "Save Edit"}
	, _newCmd{*this, "New"}
	, _delCmd{*this, "Delete"}
	, _prevCmd{*this, "<"}
	, _nextCmd{*this, ">"}
	, _typeCmbo{*this, "Type"}
	, _dialectCmbo{*this, "Dialect"}
	, _levelCmbo{*this, "Complexity"}
	, _skillCmbo{*this, "Skill"}
	, _chapterCmbo{*this, "Chap"}
	, _categoryCmbo{*this, "Category"}
	, _searchLbl{*this, "Search..."}
	, _searchBox{*this, ""}
	, _featureList{ *this }
	, _filter{ *this, "Filter..." }
	, _editTxt{ *this, "Edit entry" }
	, _notesTxt{ *this, "Your notes here..." }
	, _user{ appFolder, ".txt" }
	, _place(*this)
{
	caption("C++ Features");
	layout();
	_initialise_buttons();
	_initialise_Combos();
	_initialise_folderPath_Element();
	_initialise_fileList_Element();
	_initialise_UserList_Element();
	_initialise_featureList_Element();
	_initialise_editBox_Elements();

	_load_FileList();
	events().unload([this](const arg_unload& arg) {	_save_File(); });
	_change_behaviour.setGui(*this);
}

void GUI::layout() {
	// Divide the form into fields.
	// <> defines a field. Fields may be nested.
	// <field_name weight=n%|n vertical gap=n>
	// field_name is used to stream widgets into that field.
	// weight specifies the exact or % of the enclosing field space taken by the field.
	// Default layout is horizontal, which means <nested fields> are placed in a row. In which case weight of a nested field relates to its width.
	// For vertical enclosing fields, <nested fields> are placed in a column. In which case weight of a nested field relates to its height.
	// gap=n specifies the gap between adjacent widgets streamed into a field.
	// Gaps may be inserted between fields with the |, i.e. <field>|<onotherField>
	// arrange=[a,b,c] specifies the weight for successive widgets streamed into a field.
	char layout[] =
		"<vertical\
			<topMargin>\
			<vbody weight=95%\
				<leftMargin>\
				<hbody weight=95% vertical\
					<colums weight=70%\
						<filesColumn weight=30% vertical \
							<pathLbl weight=25>\
							<path weight=40>|\
							<fileList weight=90>|\
							<loadBtns weight=25 gap=10 >|\
							<userList>|\
							<editBtns weight=25 gap=10>\
						>|\
						<featuresColumn weight=65% vertical gap=5\
							<searchBar weight=25 gap=5 arrange=[90,45,45,variable]> | \
							<features vertical arrange=[variable,25]>\
						>\
					>|\
					<rows weight=30% vertical gap=5\
						<editFeature weight=25 arrange=[70,60,90,80,45,90]>\
					>\
				>\
				<rightMargin>\
			>\
			<bottomMargin>\
		>";

	_place.div(layout);

	//Insert widgets

	_place["pathLbl"] << _folder_lbl;
	_place["path"] << _folderPath;
	_place["fileList"] << _fileList;
	_place["loadBtns"] << _loadFileCmd << _tidyFileCmd << _saveAsCmd;
	_place["userList"] << _userList;
	_place["editBtns"] << _saveEditCmd << _newCmd << _delCmd;
	_place["searchBar"] << _searchLbl << _prevCmd << _nextCmd << _searchBox;
	_place["features"] << _featureList<< _filter;
	_place["editFeature"] << _typeCmbo << _dialectCmbo << _levelCmbo << _skillCmbo << _chapterCmbo << _categoryCmbo << _editTxt;
	_place["rows"] << _notesTxt;

	_place.collocate();
}

// ************** Connect events **************************
void GUI::_initialise_folderPath_Element() {
	_folderPath.events().key_release([this]() { _getFolderPath(); });
}

void GUI::_initialise_buttons() {
	_loadFileCmd.events().click([this]() { _load_File(); });
	_tidyFileCmd.events().click([this]() { _tidy_FeatureList(); });
	_saveAsCmd.events().click([this]() { _save_File(); });
	_saveEditCmd.events().click([this]() { _behaviour->save_cmd(*this); /*_process_edit();*/ });
	_newCmd.events().click([this]() { _behaviour->new_cmd(*this); });
	_delCmd.events().click([this]() { _behaviour->del_cmd(*this); });
	_searchBox.events().key_release([this]() { _change_behaviour.transitionTo(Change_Behaviour::Transitions::gotSearchTerm); });
	_nextCmd.events().click([this]() { _nextMatch(1); });
	_prevCmd.events().click([this]() { _nextMatch(-1); });
	_saveAsCmd.enabled(false);
}

void GUI::_initialise_Combos() {
	// _filter
	_filter.push_back("All");
	for (auto featureType : featureType_Range.valueRange()) _filter.push_back(featureType);
	_filter.option(0);
	_filter.events().selected([this]() {_load_Feature_list(); });
	_filter.events().click([this]() {_load_Feature_list(); });

	// _typeCmbo
	for (auto featureType : featureType_Range.valueRange()) _typeCmbo.push_back(featureType);
	_typeCmbo.push_back("Type");
	_typeCmbo.events().text_changed([this]() {_type_combo_changed(); });

	// _dialectCmbo
	for (auto dialect : featureCollectionForSorting->valueRange(e_dialect)) _dialectCmbo.push_back(dialect);
	_dialectCmbo.push_back("Dialect");
	_dialectCmbo.events().text_changed([this]() {_feature_combo_changed(); });

	// _levelCmbo
	for (auto level : featureCollectionForSorting->valueRange(e_complexity)) _levelCmbo.push_back(level);
	_levelCmbo.push_back("Level");
	_levelCmbo.events().text_changed([this]() {_feature_combo_changed(); });

	// _skillCmbo
	for (auto skill : featureCollectionForSorting->valueRange(e_skill)) _skillCmbo.push_back(skill);
	_skillCmbo.push_back("Skill");
	_skillCmbo.events().text_changed([this]() {_feature_combo_changed(); });

	// _chapterCmbo
	for (auto chapter : featureCollectionForSorting->valueRange(e_chapter)) _chapterCmbo.push_back(chapter);
	_chapterCmbo.push_back("Chap");
	_chapterCmbo.events().text_changed([this]() {_feature_combo_changed(); });

	// _categoryCmbo
	for (auto category : featureCollectionForSorting->valueRange(e_category)) _categoryCmbo.push_back(category);
	_categoryCmbo.push_back("Category");
	_categoryCmbo.events().text_changed([this]() {_feature_combo_changed(); });
}

void GUI::_type_combo_changed() {
	auto index = _typeCmbo.option();
	auto newType = featureType_Range.keyAt(index);
	if (!newType.empty()) {
		_categoryCmbo.clear();
		for (auto category : _user.allFeatures().featureSet(newType).valueRange(e_category)) _categoryCmbo.push_back(category);
		_categoryCmbo.push_back("Category");
	}

	_feature_combo_changed();
}

void GUI::_feature_combo_changed() {
	string editText;
	_editTxt.getline(0, editText);

	if (!editText.empty()) _behaviour->itemEdited(*this);
}

void GUI::_initialise_editBox_Elements() {
	_editTxt.events().key_release([this]() { _behaviour->itemEdited(*this); });
	_notesTxt.events().key_release([this]() { _behaviour->itemEdited(*this); });
}

// ************** Initialise Lists **************************

void GUI::_initialise_fileList_Element() {
	_fileList.append_header({ "File Name" }, 250);
	_fileList.column_at(0).fit_content();
	_fileList.enable_single(true, false);
	_fileList.events().selected([this](const nana::arg_listbox & ar_lbx) { _fileSelected(ar_lbx); });
}

void GUI::_initialise_UserList_Element() {
	_userList.append_header({ "User Info" }, 250);
	for (auto info : user_info_labels) _userList.append(info);
	_userList.at(1).append("Your name...");
	_userList.at(2).append("your file name...");
	_userList.at(3).append("courses you have attended...");
	_userList.at(4).append("projects you have worked on...");

	_userList.events().selected([this]() { _change_behaviour.transitionTo(Change_Behaviour::Transitions::userInfoSelected); });
}

void GUI::_initialise_featureList_Element() {
	_featureList.append_header({ "Type" }, 45);
	_featureList.append_header({ "Dialect" }, 45);
	_featureList.append_header({ "Level" }, 40);
	_featureList.append_header({ "Skill" }, 40);
	_featureList.append_header({ "Chap" }, 40);
	_featureList.append_header({ "Category" }, 80);
	_featureList.append_header({ "Feature" }, 350);
	_featureList.append_header({ "Notes" }, 50);
	_featureList.column_at(e_notes).visible(false);
	_featureList.enable_single(true, false);

	_featureList.set_sort_compare(e_type, compareType);
	_featureList.set_sort_compare(e_complexity, compareLevel);
	_featureList.set_sort_compare(e_skill, compareSkill);
	_featureList.set_sort_compare(e_chapter, compareChapter);
	_featureList.set_sort_compare(e_category, compareCategory);
	_featureList.set_sort_compare(e_feature, compareFeature);
	_featureList.column_at(e_feature).fit_content();
	_featureSelectEvent = _featureList.events().selected([this]() { _change_behaviour.transitionTo(Change_Behaviour::Transitions::featureSelected); });
	_featureSelectEvent = _featureList.events().click([this]() { _change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted); });
}

// event handlers

void GUI::_fileSelected(const nana::arg_listbox & ar_lbx) {
	auto item = ar_lbx.item.text(0);
	if (item.find(USER_FILE_POSTSCRIPT) < item.npos) {
		_loadFileCmd.caption("Load User");
	} else _loadFileCmd.caption("Load File");
	_loadFileCmd.enabled(true);
}

void GUI::_catchUser() {
	if (_userList.selected().size() > 0) {
		_notesTxt.reset();
		auto cat = _userList.selected().front().cat;
		for (auto line : _userList.at(cat)) {
			_notesTxt.append(line.text(0) + '\n', false);
		}
	}
}

void GUI::_clearFeaturesSelection() {
	for (auto item : _featureList.selected())
		_featureList.at(item).select(false);
}

void GUI::_catchFeature () {
	static nana::listbox::index_pair currentPos;
	if (!_featureList.selected().empty()) {
		if(currentPos == _featureList.selected().front()) currentPos = _featureList.selected().back();
		else currentPos = _featureList.selected().front();
		_clearFeaturesSelection();
		_setFeaturesSelection(currentPos, true);
		_copyToEditBox(currentPos);
	}
}

void GUI::_setFeaturesSelection(nana::listbox::index_pair pos, bool showThis) {
	if (_featureList.at(pos.cat).size() > 0) {
		if (showThis) _featureList.at(pos).select(false, false); // selection state must be changed to re-trigger scroll to show.
		_featureList.at(pos).select(true, showThis);
	}
}

void GUI::_search() {
	_featureList.auto_draw(false);
	_featureList.disable_single(true);
	_clearFeaturesSelection();
	string searchText{};
	_searchBox.getline(0, searchText);

	auto lowerSearchText = stoLower(searchText);
	bool showThis = true;
	for (auto item : _featureList.at(0)) {
		if (stoLower(item.text(e_feature)).find(lowerSearchText) != string::npos) {
			_setFeaturesSelection(item.pos(), showThis);
			showThis = false;
		}
	}
	if (_featureList.selected().empty()) {
		_editTxt.reset(searchText);
		_notesTxt.reset();
	}
	else if (_featureList.selected().size() == 1) {
		_copyToEditBox(_featureList.selected()[0]);
	}
	stringstream ss;
	ss << "Search...(" << _featureList.selected().size() << ")";
	_searchLbl.caption(ss.str());

	_nextMatch(0, true);
	_featureList.auto_draw(true);
}

void GUI::_nextMatch(int direction, bool reset) {
	static auto currentPos = 0;
	
	int noSelected = int(_featureList.selected().size());
	if (reset) currentPos = 0;
	else if(_nextCmd.caption() == ">") _change_behaviour.transitionTo(Change_Behaviour::Transitions::gotSearchTerm);
	if (noSelected > 0) {
		currentPos = std::clamp(currentPos += direction, 0, noSelected-1);	
		auto pos = _featureList.selected().at(currentPos);
		_setFeaturesSelection(pos, true);
		_copyToEditBox(pos);
	}
	else noSelected = 1;

	{stringstream ss;
	ss << ">" << noSelected - currentPos - 1;
	_nextCmd.caption(ss.str());
	}
	{stringstream ss;
	ss << currentPos << "<";
	_prevCmd.caption(ss.str());
	}
}

// helpers

void GUI::_copyToEditBox(nana::listbox::index_pair listPos) {
	auto line = _featureList.at(listPos);
	_editTxt.reset(); // to signal to _feature_combo_changed not to trigger a save.
	
	auto featureType = line.text(e_type);
	auto pos = featureType_Range.rangePos(featureType);
	_typeCmbo.option(pos);

	auto & featureSet = _user.allFeatures().featureSet(featureType);

	pos = featureSet.rangePos(e_dialect, line.text(e_dialect));
	_dialectCmbo.option(pos);

	pos = featureSet.rangePos(e_complexity, line.text(e_complexity));
	_levelCmbo.option(pos);

	pos = featureSet.rangePos(e_skill, line.text(e_skill));
	_skillCmbo.option(pos);

	pos = featureSet.rangePos(e_chapter, line.text(e_chapter));
	_chapterCmbo.option(pos);

	_categoryCmbo.clear();
	for (auto category : featureSet.valueRange(e_category)) _categoryCmbo.push_back(category);
	_categoryCmbo.push_back("Category");

	pos = featureSet.rangePos(e_category, line.text(e_category));
	_categoryCmbo.option(pos);

	_editTxt.reset(line.text(e_feature));
	string notes = line.text(e_notes);
	replace(notes.begin(), notes.end(), '\10', '\n'); // replace all 'x' to 'y'notes.
	_notesTxt.reset(notes);
}

void GUI::_saveEdit() {
	auto pos = _featureList.selected().at(0);
	auto originalFeature = getFeature(pos);
	auto editedFeature = getFeatureFromEdit();
	_user.allFeatures().editFeature(originalFeature, editedFeature);
	setFeature(pos, editedFeature);
}

void GUI::_saveNew() {
	appendFeature(getFeatureFromEdit());
	auto pos = _featureList.at(0).back().pos();
	_setFeaturesSelection(pos, true);
}

// ************** File-List Functions **************************

void GUI::_getFolderPath() {
	string path{};
	_folderPath.getline(0, path);
	_user.files().setPath(path);
	_load_FileList();
}

void GUI::_load_FileList() {
	_user.files().getFilePaths();
	_fileList.clear();
	for (auto file : _user.files().filePaths()) {
		_fileList.at(0).append(file.filename().string());
	}
	if (_fileList.at(0).size() > 0) {
		auto userFeaturesFileName = _user.getField(e_featuresFileName)[0];
		if (userFeaturesFileName.empty()) {
			_fileList.at(0).at(0).select(true);
		}
		else {
			userFeaturesFileName += ".txt";
			auto pos = find_if(_fileList.at(0).begin(), _fileList.at(0).end(), [userFeaturesFileName](auto item) {return item.text(0) == userFeaturesFileName /*.find(userFeaturesFileName) != npos*/; });
			if (pos != _fileList.at(0).end()) {
				pos.select(true);
				_load_File();
			}
		}
	}
}

// ************** Feature-List Functions **************************

void GUI::_load_File() {
	if (!_fileList.selected().empty() ) {
		auto filePos = _fileList.selected()[0].item;
		if (_loadFileCmd.caption() == "Load User") {
			_user.load_InfoFile(filePos);
			_userList.auto_draw(false);
			_userList.clear();
			_userList.at(e_userName + 1).append(_user.getField(e_userName)[0]);
			_userList.at(e_featuresFileName + 1).append(_user.getField(e_featuresFileName)[0]);
			auto item = _userList.at(e_cpp_courses + 1);
			for (auto line : _user.getField(e_cpp_courses)) { item.append(line); }
			item = _userList.at(e_projects + 1);
			for (auto line : _user.getField(e_projects)) { item.append(line); }
			_userList.auto_draw(true);
			_load_FileList();
		}
		else {
			_user.load_FeaturesFile(filePos);
			_load_Feature_list();
		}
	}
	_loadFileCmd.enabled(false);
	_saveAsCmd.enabled(false);
	_change_behaviour.transitionTo(Change_Behaviour::Transitions::featureSelected);
}

void GUI::_load_Feature_list() {
	// lambda
	auto _append_Features = [this](auto features) {
		for (auto feature : features) {
			_featureList.at(0).append({ feature[e_type] });
			for (int i = e_dialect; i < NO_OF_COLUMNS; ++i) {
				_featureList.at(0).back().text(i, feature[i]);
			}
		}
	};

	// algorithm
	_featureList.auto_draw(false);
	_featureList.clear();
	auto filterPos = _filter.option();
	string featureType;
	if (filterPos) {
		featureType = featureType_Range.keyAt(filterPos-1);
	}
	else featureType = ALL_FEATURES;

	auto features = _user.allFeatures().features(featureType);
	_append_Features(features);

	_setFeaturesSelection(listbox::index_pair{ 0,0 }, true);
	_featureList.auto_draw(true);
}

vector<string> GUI::getFeature(listbox::index_pair pos) const {
	vector<string> feature;
	if (_featureList.at(pos.cat).size() == 0) return {};
	auto line = _featureList.at(pos);
	for (int f = e_type; f != NO_OF_COLUMNS; ++f) feature.push_back(line.text(f));
	return feature;
}

void GUI::setFeature(listbox::index_pair pos, std::vector<std::string> feature) {
	auto line = _featureList.at(pos);
	for (int f = e_type; f != NO_OF_COLUMNS; ++f) line.text(f, feature[f]);
}

std::vector<std::string> GUI::getFeatureFromEdit() const {
	std::vector<std::string> editedFeature(NO_OF_COLUMNS);

	string featureType;
	auto index = _typeCmbo.option();
	if (index < featureType_Range.size()) {
		featureType = featureType_Range.keyAt(index);
		editedFeature.at(e_type) = featureType;
	}
	auto & featureSet = _user.allFeatures().featureSet(featureType);
	editedFeature.at(e_dialect) = featureSet.keyAt(e_dialect, _dialectCmbo.option());
	editedFeature.at(e_complexity) = stoLower(featureSet.keyAt(e_complexity, _levelCmbo.option()));
	editedFeature.at(e_skill) = stoLower(featureSet.keyAt(e_skill, _skillCmbo.option()));
	editedFeature.at(e_chapter) = featureSet.keyAt(e_chapter, _chapterCmbo.option());
	editedFeature.at(e_category) = featureSet.keyAt(e_category, _categoryCmbo.option());

	string feature;
	ostringstream newNote;
	_editTxt.getline(0, feature);

	for (int i = 0; i < _notesTxt.text_line_count(); ++i) {
		string noteLine;
		_notesTxt.getline(i, noteLine);
		newNote  << noteLine <<  '\10';
	}

	editedFeature.at(e_feature) = feature;
	editedFeature.at(e_notes) = newNote.str();
	return editedFeature;
}

void GUI::_tidy_FeatureList() {
	_user.allFeatures().tidy_features();
	_load_Feature_list();
	_saveAsCmd.enabled(true);
}

void GUI::_save_File() {
	_user.saveAllFeatures();
	_load_FileList();
	_saveAsCmd.enabled(false);
}

bool GUI::_save_user_info() {
	if (!_userList.selected().empty()) {
		auto pos = _userList.selected()[0];
		_userList.clear(pos.cat);
		auto cat = _userList.at(pos.cat);
		vector<string> newInfo(_notesTxt.text_line_count());
		for (int i = 0; i < _notesTxt.text_line_count(); ++i) {
			_notesTxt.getline(i, newInfo[i]);
			cat.append(newInfo[i]);
		}
		_user.setField(E_UserInfo(pos.cat - 1), newInfo);
		_user.saveInfoFile();
		_notesTxt.reset();
		_load_FileList();
		return !_userList.at(pos).text(0).empty();
	}
	return false;
}

void GUI::appendFeature(vector<string> newFeature ) {
	_user.allFeatures().appendFeature(newFeature);
	_featureList.at(0).append({ newFeature[e_type] });
	for (int i = e_dialect; i < NO_OF_COLUMNS; ++i) {
		_featureList.at(0).back().text(i, newFeature[i]);
	}
}

void GUI::eraseFeature() {
	nana::drawerbase::listbox::index_pair pos;
	if (_featureList.selected().size() == 1) {
		pos = _featureList.selected().at(0);
		auto originalFeature = getFeature(pos);
		_user.allFeatures().eraseFeature(originalFeature);
		auto item = _featureList.at(pos);
		_featureList.erase(item);
	}

	if (pos.item >= _featureList.at(0).size()) pos = _featureList.at(0).back().pos();
	_setFeaturesSelection(pos, true);
	_copyToEditBox(pos);
}

// Objects for State
GUI::Change_Behaviour GUI::_change_behaviour;

void GUI::_changeState(I_Behaviour * newBehaviour) {
	if (_behaviour != newBehaviour) {
		_behaviour = newBehaviour;
		_behaviour->setGUI_state(*this);
	}
}

void GUI::Change_Behaviour::setGui(GUI & gui) {
	_gui = &gui;
	transitionTo(Transitions::initialise);
}

void GUI::Change_Behaviour::transitionTo(Transitions transition) const {
	static bool doingTransition = false;
	if (doingTransition) 
		return;
	doingTransition = true;
	switch (transition) {
	case Transitions::initialise: _gui->_changeState(&_select); break;
	case Transitions::featureSelected:
		if (_gui->_behaviour == &_search) break;
		if (_gui->_behaviour == &_newFeature) break;
	case Transitions::editFeatureCompleted: 
	case Transitions::userInfoCompleted:
		_gui->_changeState(&_select);
		_gui->_catchFeature();
		_gui->_behaviour->setDel_enable(*_gui);
		break;
	case Transitions::gotSearchTerm:
	{
		string searchText;
		_gui->_searchBox.getline(0, searchText);
		if (!searchText.empty()) {
			_gui->_changeState(&_search);
			_gui->_search();
		}
		else {
			_gui->_changeState(&_select);
			transitionTo(Transitions::featureSelected);
		}
		break;
	}
	case Transitions::featureEdited:
		if (_gui->_behaviour == &_newFeature) break;
		if (_gui->_featureList.selected().size() == 0) break;
		_gui->_changeState(&_editFeature); break;
	case Transitions::newFeatureRequested: _gui->_changeState(&_newFeature); break;
	case Transitions::userInfoSelected: 
		_gui->_changeState(&_viewUser);
		_gui->_catchUser();
		break;
	case Transitions::userInfoEdited: _gui->_changeState(&_editUser); break;
	}
	doingTransition = false;
}

void GUI::I_Behaviour::setDel_enable(GUI & gui) const {
	gui._delCmd.enabled(gui._featureList.selected().size() == 1);
}

void GUI::I_Behaviour::itemEdited(GUI & gui) const {
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::featureEdited);
}

// Select_Behaviour
void GUI::Select_Behaviour::setGUI_state(GUI & gui) const {
	gui._nextCmd.caption(">");
	gui._prevCmd.caption("<");
	gui._searchLbl.caption("Search...");
	gui._saveEditCmd.caption("Save Edit");
	gui._saveEditCmd.enabled(false);
	gui._newCmd.enabled(true);
	gui._delCmd.caption("Delete");
	gui._editTxt.enabled(true);
	gui._featureList.disable_single(false);
	gui._featureList.enable_single(true, true);

	nana::listbox::index_pair pos;
	if (!gui._featureList.empty()) {
		if (gui._featureList.selected().empty()) {
			if (gui._featureList.at(0).size() > 0) {
				pos = gui._featureList.at(0).at(0).pos();
			}
		}
		else {
			pos = gui._featureList.selected().front();
		}
		gui._setFeaturesSelection(pos, true);
	}
	setDel_enable(gui);
}

void GUI::Select_Behaviour::new_cmd(GUI & gui) const {
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::newFeatureRequested);
}

void GUI::Select_Behaviour::del_cmd(GUI & gui) const {
	gui.eraseFeature();
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted);
}

// Search_Behaviour
void GUI::Search_Behaviour::setGUI_state(GUI & gui) const {
	setDel_enable(gui);
	gui._saveEditCmd.caption("Save Edit");
	gui._saveEditCmd.enabled(false);
	gui._newCmd.enabled(true);
	gui._delCmd.caption("Delete");
	gui._featureList.disable_single(true);
}

void GUI::Search_Behaviour::new_cmd(GUI & gui) const {
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::newFeatureRequested);
}

void GUI::Search_Behaviour::del_cmd(GUI & gui) const {
	gui.eraseFeature();
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted);
}

// EditFeature_Behaviour
void GUI::EditFeature_Behaviour::setGUI_state(GUI & gui) const {
	setDel_enable(gui);
	gui._nextCmd.caption(">");
	gui._prevCmd.caption("<");
	gui._featureList.disable_single(false);
	gui._saveEditCmd.enabled(true);
	gui._delCmd.caption("Cancel");
}

void GUI::EditFeature_Behaviour::save_cmd(GUI & gui) const {
	gui._saveEdit();
	gui._saveAsCmd.enabled(true);
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted);
}

void GUI::EditFeature_Behaviour::new_cmd(GUI & gui) const {
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::newFeatureRequested);
}

void GUI::EditFeature_Behaviour::del_cmd(GUI & gui) const { // cancel edit
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted);
}

// NewFeature_Behaviour
void GUI::NewFeature_Behaviour::setGUI_state(GUI & gui) const {
	gui._nextCmd.caption(">");
	gui._prevCmd.caption("<");
	gui._featureList.disable_single(false);
	gui._clearFeaturesSelection();
	gui._saveEditCmd.caption("Save New");
	gui._saveEditCmd.enabled(true);
	gui._newCmd.enabled(false);
	gui._delCmd.caption("Cancel");
	gui._delCmd.enabled(true);
}

void GUI::NewFeature_Behaviour::del_cmd(GUI & gui) const { // cancel new
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted);
}

void GUI::NewFeature_Behaviour::save_cmd(GUI & gui) const {
	gui._saveNew();
	gui._saveAsCmd.enabled(true);
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::editFeatureCompleted);
}

// ViewUserInfo_Behaviour
void GUI::ViewUserInfo_Behaviour::setGUI_state(GUI & gui) const {
	gui._saveEditCmd.caption("Save Info");
	gui._delCmd.caption("Cancel");
	gui._saveEditCmd.enabled(false);
	gui._newCmd.enabled(false);
	gui._delCmd.enabled(false);
	gui._editTxt.enabled(false);
	gui._editTxt.reset();
}

void GUI::ViewUserInfo_Behaviour::itemEdited(GUI & gui) const {
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::userInfoEdited);
}

// EditUserInfo_Behaviour
void GUI::EditUserInfo_Behaviour::setGUI_state(GUI & gui) const {
	gui._saveEditCmd.enabled(true);
	gui._newCmd.enabled(false);
	gui._delCmd.enabled(true);
}

void GUI::EditUserInfo_Behaviour::save_cmd(GUI & gui) const {
	if (gui._save_user_info()) gui._saveAsCmd.enabled(true);
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::userInfoCompleted);
}

void GUI::EditUserInfo_Behaviour::del_cmd(GUI & gui) const { // cancel edit
	gui._change_behaviour.transitionTo(Change_Behaviour::Transitions::userInfoCompleted);
}
