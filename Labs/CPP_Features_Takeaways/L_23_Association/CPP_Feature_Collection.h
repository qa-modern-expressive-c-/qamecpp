#pragma once
#include"CPP_Feature.h"
#include <string>
#include <vector>

namespace cpp_features {
	/////////////// Feature Collection Class /////////////
	class I_Option_Range;

	class CPP_Feature_Collection : public std::vector<CPP_Language_Feature> {
	public:
		//using std::vector<CPP_Language_Feature>::vector;
		CPP_Feature_Collection(
			std::shared_ptr<I_Option_Range> dialect_range
			, std::shared_ptr<I_Option_Range> complexity_range
			, std::shared_ptr<I_Option_Range> skill_range
			, std::shared_ptr<I_Option_Range> chapter_range
			, std::shared_ptr<I_Option_Range> category_range
		);
		CPP_Feature_Collection() = default;

		// Queries
		auto optionRangeFor(TSV_Fields) const -> const I_Option_Range &;
		auto keyRange(TSV_Fields) const->std::vector<std::string>;
		auto valueRange(TSV_Fields) const->std::vector<std::string>;
		auto rangePos(TSV_Fields, std::string fieldKey) const->std::size_t;
		auto keyAt(TSV_Fields, std::size_t pos) const->std::string;
		auto featureAt(size_t pos) const -> std::vector<std::string> { return at(pos).asVector(); }

		// Modifiers
		auto findFeature(std::vector<std::string> feature)->std::vector<CPP_Language_Feature>::iterator;
		void addFeature(std::vector<std::string> arg);
		void editFeature(size_t pos, std::vector<std::string> edit);
		void eraseFeature(size_t pos);
	private:
		friend class CPP_All_Features_Collections;
		std::shared_ptr<I_Option_Range> _dialect_range;
		std::shared_ptr<I_Option_Range> _complexity_range;
		std::shared_ptr<I_Option_Range> _skill_range;
		std::shared_ptr<I_Option_Range> _chapter_range;
		std::shared_ptr<I_Option_Range> _category_range;
	};
}

