#include "pch.h"
#include"fileHandler.h"
#include <fstream>
#include <sstream>

using namespace std;
namespace fs = std::filesystem;

FileHandler::FileHandler(const fs::path & rootPath, const fs::path & extention)
	: _rootPath{ rootPath }
	, _extension{ extention }
{}

void FileHandler::setPath(std::string path) {
	_rootPath = path;
}

void FileHandler::getFilePaths() {
	_filePaths = getFilePaths(_rootPath, _extension);
}

auto FileHandler::getFilePaths(const fs::path & pathToShow, const fs::path & extention) -> vector<fs::path> {
	auto fileList = vector<fs::path>{};
	for (auto const & entry : fs::directory_iterator{ pathToShow }) {
		if (entry.path().extension() == extention) {
			fileList.push_back(entry.path());
		}
	}
	return fileList;
}

 auto FileHandler::getLinesFromFileAt(size_t fileSelectionIndex) const -> vector<string> {
	auto filePath = filePaths().at(fileSelectionIndex);
	auto fileLines = vector<string>{};
	ifstream readableFile{ filePath };
	while (readableFile.good()) {
		string line{};
		getline(readableFile, line);
		fileLines.push_back(line);
	}
	 return fileLines;
 }

 void FileHandler::writeFile(const std::filesystem::path & fileName, const std::vector<std::string> & lines) {
	 auto fullPath = _rootPath / fileName.stem() += _extension;
	 ofstream writableFile{ fullPath };
	 if (writableFile.good()) {
		 for (const auto & line : lines) {
			 writableFile << line << endl;
		 }
	 }
 }