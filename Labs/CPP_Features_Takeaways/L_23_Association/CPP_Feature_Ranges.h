#pragma once

#include <string>
#include <deque>
#include <vector>				 
#include <initializer_list>

namespace cpp_features {

	class I_Option_Range {
	public:
		// Queries
		virtual auto keyRange() const->std::vector<std::string> = 0;
		virtual auto valueRange() const->std::vector<std::string> = 0;
		virtual auto keyAt(size_t pos) const->std::string = 0;
		virtual auto rangePos(std::string fieldKey) const->std::size_t = 0;
		virtual auto keyMatches(std::string item) const->std::string = 0;
		virtual ~I_Option_Range() = default;
	};

	class Val_Option_Range : public I_Option_Range, public std::deque<std::string> {
	public:
		using std::deque<std::string>::deque;
		// Queries
		auto keyRange() const->std::vector<std::string> override;
		auto valueRange() const -> std::vector<std::string> override { return keyRange(); }
		auto keyAt(size_t pos) const->std::string override;
		auto rangePos(std::string fieldKey) const -> std::size_t override;
		auto keyMatches(std::string item) const->std::string override;
	};

	class Key_Val_Option_Range : public I_Option_Range, public std::deque<std::pair<char, std::string>> {
	public:
		using std::deque<std::pair<char, std::string>>::deque;
		Key_Val_Option_Range(std::initializer_list<std::string>);
		// Queries
		auto keyRange() const->std::vector<std::string>  override;
		auto valueRange() const->std::vector<std::string> override;
		auto keyAt(size_t pos) const->std::string override;
		auto rangePos(std::string fieldKey) const -> std::size_t override;
		auto keyMatches(std::string item) const->std::string override;
	};

	auto createSequentialRange(std::string prefix, size_t noOfElements)->Val_Option_Range;
}