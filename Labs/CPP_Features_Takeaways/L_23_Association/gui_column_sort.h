#pragma once
#include <string>
#include <nana/gui/widgets/listbox.hpp>

bool compareType(const std::string & text_l, nana::any *, const std::string & text_r, nana::any *, bool reverse);
bool compareLevel(const std::string & text_l, nana::any *, const std::string & text_r, nana::any *, bool reverse);
bool compareCategory(const std::string & text_l, nana::any *, const std::string & text_r, nana::any *, bool reverse);
bool compareSkill(const std::string & text_l, nana::any *, const std::string & text_r, nana::any *, bool reverse);
bool compareChapter(const std::string & text_l, nana::any *, const std::string & text_r, nana::any *, bool reverse);
bool compareFeature(const std::string & text_l, nana::any *, const std::string & text_r, nana::any *, bool reverse);

