#include "pch.h"
#include "CPP_All_Features_Collection.h"
#include "CPP_Global_Constants.h"
#include "CPP_Feature_Ranges.h"							   
#include "string_utilities.h"
#include <memory>				 
#include <initializer_list>


namespace cpp_features {
	using namespace string_utilities;
	using namespace std;
	using iniList = initializer_list<string>;

	CPP_All_Features_Collections::CPP_All_Features_Collections()
		:
		_keyword_features{
			  make_shared<Val_Option_Range>(iniList{"03","11","14","17","20", "23"})
			, make_shared<Key_Val_Option_Range>(iniList{"Basic","Intermediate","Advanced","eXpert"})
			, make_shared<Key_Val_Option_Range>(iniList{"Zero", "Aware", "Basic","Competent"})
			, make_shared<Val_Option_Range>(createSequentialRange("E",23))
			, make_shared<Val_Option_Range>(iniList{
				  "Declaration"
				, "Initialisation"
				, "Operations"
				, "Behaviour"
				, "Flow Control"
				, "UDT"
				, "Conversion"
				, "Scope"
				, "Lifetimes"
				, "Literal"
				, "Templates"
				, "Errors"
			})
		}
		, _language_features{ _keyword_features }
		, _class_features{ _keyword_features }
		, _STL_Features{
			  _keyword_features._dialect_range
			, _keyword_features._complexity_range
			, _keyword_features._skill_range
			, _keyword_features._chapter_range
			, make_shared<Val_Option_Range>(iniList{
				  "Container"
				, "Algorithm"
				, "Generics"
				, "Utility"
				, "Memory"
				, "Types/Information"
				, "Multi-Threading"
				, "Date/Time/Locale"
				, "Input/Output"
				, "Numerics"
				, "Strings"
			})
		}
		, _build_features{
			  _keyword_features._dialect_range
			, _keyword_features._complexity_range
			, _keyword_features._skill_range
			, _keyword_features._chapter_range
			, make_shared<Val_Option_Range>(iniList{
				  "Messages"
				, "Dependancies"
				, "Attributes"
			})
		}

		, _advice{
			  _keyword_features._dialect_range
			, _keyword_features._complexity_range
			, _keyword_features._skill_range
			, _keyword_features._chapter_range
			, make_shared<Val_Option_Range>(iniList{
				  "Idiom"
				, "Coding Guidline"
				, "Design Pattern"
				, "Design Principle"
			})
		}
		{ featureCollectionForSorting = &_keyword_features;	}

	vector<vector<string>> CPP_All_Features_Collections::features(string featureTypeKey) const {
		vector<vector<string>> featureList;
		// Lambda
		auto appendFeatureSet = [&featureList, this](auto featureType) {
			auto & feature_set = featureSet(featureType);
			for (auto language_feature : feature_set) {
				featureList.push_back(language_feature.asVector());
			}
		};

		// algorithm
		if (featureTypeKey == ALL_FEATURES) {
			for (auto key : featureType_Range.keyRange()) appendFeatureSet(key);
		}
		else {
			appendFeatureSet(featureTypeKey);
		}
		return featureList;
	}

	void CPP_All_Features_Collections::assignFeature(std::string tsv_line) {
		auto splitLine = splitString(tsv_line, '\t', NO_OF_COLUMNS);
		auto type_key = splitLine.at(e_type);
		if (!type_key.empty()) {
			featureSet(type_key).addFeature(splitLine);
		}
	}
	   
	void CPP_All_Features_Collections::clear() {
		for (auto key : featureType_Range.keyRange()) {
			auto & feature_set = featureSet(key);
			feature_set.clear();
		}
	}

	void CPP_All_Features_Collections::tidy_features() {
		// lambdas
		auto setMissingDialect = [](auto & feature) {
			if (feature[e_dialect].empty()) {
				feature[e_dialect] = "03";
			}
		};

		auto trimQuotes = [](auto & feature) {
			feature[e_feature] = trimChar(feature[e_feature], '"');
			feature[e_notes] = trimChar(feature[e_notes], '"');
		};

		auto doTidy = [setMissingDialect, trimQuotes](auto feature) {
			setMissingDialect(feature);
			trimQuotes(feature);
			return feature;
		};

		// Algorithm
		for (auto featureTypeKey : featureType_Range.keyRange()) {
			auto & feature_set = featureSet(featureTypeKey);
			for (auto featureIT = feature_set.begin(); featureIT != feature_set.end(); ++featureIT) {
				auto pos = distance(feature_set.begin(),featureIT);
				feature_set.editFeature(pos, doTidy(featureIT->asVector()));
			}
		}
	}

	void CPP_All_Features_Collections::appendFeature(std::vector<std::string> newFeature) {
		auto & feature_set = featureSet(newFeature[e_type]);
		feature_set.addFeature(newFeature);
	}

	void CPP_All_Features_Collections::eraseFeature(std::vector<std::string> original) {
		auto & feature_set = featureSet(original[e_type]);
		auto foundIT = feature_set.findFeature(original);
		feature_set.eraseFeature(distance(feature_set.begin(), foundIT));
	}

	void CPP_All_Features_Collections::editFeature(vector<string> original, vector<string> edit) {
		auto feature_set = &featureSet(original[e_type]); // needs to be re-assignable, so can't use a reference
		auto foundIT = feature_set->findFeature(original);
		auto foundPos = distance(feature_set->begin(), foundIT);
		if (foundIT != feature_set->end()) {
			if (original[e_type] != edit[e_type]) {
				feature_set->eraseFeature(foundPos);
				feature_set = &featureSet(edit[e_type]);
				feature_set->addFeature(edit);
			}
			else {
				feature_set->editFeature(foundPos, edit);
			}
		}
	}

	CPP_Feature_Collection & CPP_All_Features_Collections::featureSet(std::string featureTypeKey) {
		switch (featureTypeKey[0]) {
		case 'K':
			return _keyword_features;
		case 'L':
			return _language_features;
		case 'C':
			return _class_features;
		case 'S':
			return _STL_Features;
		case 'B':
			return _build_features;
		case 'A':
			return _advice;
		default: return _keyword_features;
		}
	}

}
