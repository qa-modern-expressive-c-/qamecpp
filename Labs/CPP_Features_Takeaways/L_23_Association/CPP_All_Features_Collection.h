#pragma once
#include "CPP_Feature_Collection.h"
#include <vector>
#include <string>

namespace cpp_features {
	constexpr char ALL_FEATURES[] = "";

	class CPP_All_Features_Collections {
	public:
		CPP_All_Features_Collections();
		// Queries
		auto featureSet(std::string featureTypeKey = ALL_FEATURES) const -> const CPP_Feature_Collection & { return const_cast<CPP_All_Features_Collections *>(this)->featureSet(featureTypeKey); }
		auto features(std::string featureTypeKey) const -> std::vector<std::vector<std::string>>;

		// Modifiers
		void tidy_features();
		auto featureSet(std::string featureTypeKey = ALL_FEATURES) -> CPP_Feature_Collection &;
		void assignFeature(std::string tsv_line);
		void editFeature(std::vector<std::string> original, std::vector<std::string> edit);
		void eraseFeature(std::vector<std::string> original);
		void appendFeature(std::vector<std::string> newFeature);
		void clear();
	
	private:
		CPP_Feature_Collection _keyword_features;
		CPP_Feature_Collection _language_features;
		CPP_Feature_Collection _class_features;
		CPP_Feature_Collection _STL_Features;
		CPP_Feature_Collection _build_features;
		CPP_Feature_Collection _advice;
	};
}
