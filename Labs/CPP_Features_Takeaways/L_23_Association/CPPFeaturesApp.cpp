#include "pch.h"
#include "gui.h"	// user-defined gui definition
// nana GUI Creation Headers...
#include <nana/gui/wvl.hpp> // for nana::exec()
// STL Headers...
#include <filesystem>
#ifdef _DEBUG 
#define CATCH_CONFIG_RUNNER
#include <catch.hpp>
#endif	
namespace fs = std::filesystem;

int main()
{
#ifdef _DEBUG 
	Catch::Session session; // There must be exactly once instance
	session.run();
#endif	
	GUI gui{ fs::current_path().parent_path().parent_path().parent_path() /= "Delegate Projects" };
	gui.show();

	nana::exec();
}