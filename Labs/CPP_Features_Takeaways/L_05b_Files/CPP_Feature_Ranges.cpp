#include "pch.h"
#include "CPP_Feature_Ranges.h"
#include "string_utilities.h"

namespace cpp_features {
	using namespace std;
	///////////  Val_Option_Range ///////////////////

	auto Val_Option_Range::keyRange() const -> std::vector<std::string> {
		return { begin(), end() };
	}

	auto Val_Option_Range::rangePos(std::string fieldKey) const->std::size_t {
		return 0;
	}

	auto Val_Option_Range::keyAt(size_t pos) const -> std::string {
		return "";
	}

	auto Val_Option_Range::keyMatches(std::string item) const->std::string {
		for (auto & validItem : *this) {
			if (validItem == item) return item;
		}
		return { "" };
	}
}


