#include "pch.h"
#define CATCH_CONFIG_RUNNER
#include <catch.hpp>

#include "gui.h"	// user-defined gui definition
// nana GUI Creation Headers...
#include <nana/gui/wvl.hpp> // for nana::exec()
// STL Headers...
#include <filesystem>

namespace fs = std::filesystem;

int main()
{
	Catch::Session session; // There must be exactly once instance
	session.run();	
	
	GUI gui{ fs::current_path().parent_path().parent_path().parent_path() /= "Delegate Projects" };
	gui.show();

	nana::exec();
}