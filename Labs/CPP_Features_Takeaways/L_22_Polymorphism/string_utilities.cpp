#include "pch.h"
#include "string_utilities.h"
#include <sstream>
#include <numeric>
#include <algorithm>

namespace  string_utilities {
	using namespace std;

	std::string stoLower(std::string input) {
		for_each(input.begin(), input.end(), [](auto & ch) {ch = tolower(ch); });
		return input;
	}

	auto splitString(const string & source, char separator, size_t numberOfTokens) -> vector<string> {
		stringstream source_stream{ source };
		vector<string> tokens{};
		string token{};
		for (auto remainingTokens = numberOfTokens; remainingTokens > 1; --remainingTokens) {
			if (source_stream.good()) {
				getline(source_stream, token, separator);
				tokens.push_back(token);
			}
			else break;
		}
		if (source_stream.good()) { // get all the rest of the line as the last token
			getline(source_stream, token);
			tokens.push_back(token);
		}
		if (numberOfTokens != -1) {
			while (tokens.size() < numberOfTokens) tokens.push_back("");
		}
		return tokens;
	}
	
	string trimChar(const string & source, char trim_char) {
		auto start = source.find_first_not_of(trim_char);
		size_t end = 0;
		if (start == string::npos) start = 0;
		else {
			end = source.find_last_not_of(trim_char) + 1;
		}
		return source.substr(start, end - start);
	}

	auto toSV(const vector<string> items, char separator) -> string {
		if (items.size() == 0) return "";

		// lambdas
		auto addStringLength = [](auto acc, auto & word) {return acc + word.length() + 1; };

		auto fold = [separator](auto & resultStr, auto & rightStr) -> auto & {
			return resultStr.append(rightStr).append({ separator });
		};

		// algorithm
		auto length = reduce(items.begin(), items.end(), size_t(0), addStringLength);
		string tsv{};
		tsv.reserve(length);
		tsv = accumulate(items.begin(), items.end(), tsv, fold);
		tsv.pop_back();
		return tsv;
	}
};

