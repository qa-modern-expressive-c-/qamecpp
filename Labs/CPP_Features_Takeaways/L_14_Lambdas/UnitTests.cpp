#include "pch.h"
#include <catch.hpp>
#include "string_utilities.h"
#include <string>

using namespace std;
using namespace string_utilities;

// Lab 10 Unit Tests

TEST_CASE("Each Column Present", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("First Column Missing", "[SplitString]") {
	auto sourceString = string{ ",b,c,d" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("Last Column Missing", "[SplitString]") {
	auto sourceString = string{ "a,b,c" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("More Columns than Expected", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d,e" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d,e");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("Unlimited Columns", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d,e,f,g" };
	auto tokenized = splitString(sourceString, ',');
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
	CHECK(tokenized[4] == "e");
	CHECK(tokenized[5] == "f");
	CHECK(tokenized[6] == "g");
	CHECK(tokenized.size() == 7);
}

// Lab 14 Lambdas

TEST_CASE("No trims", "[TrimChar]") {
	auto test = string{ "test" };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Front trim", "[TrimChar]") {
	auto test = string{ "  test" };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Back trim", "[TrimChar]") {
	auto test = string{ "test  " };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Both-end trim", "[TrimChar]") {
	auto test = string{ "  test  " };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Trim all chars away", "[TrimChar]") {
	auto test = string{ "   " };
	CHECK(trimChar(test, ' ') == "");
}

TEST_CASE("Trim Empty Str", "[TrimChar]") {
	auto test = string{ "" };
	CHECK(trimChar(test, ' ') == "");
}