#include "pch.h"
#include "string_utilities.h"
#include <sstream>

namespace  string_utilities {
	using namespace std;

	auto splitString(const string & source, char separator, size_t numberOfTokens) -> vector<string> {
		stringstream source_stream{ source };
		vector<string> tokens{};
		string token{};
		for (auto remainingTokens = numberOfTokens; remainingTokens > 1; --remainingTokens) {
			if (source_stream.good()) {
				getline(source_stream, token, separator);
				tokens.push_back(token);
			}
			else break;
		}
		if (source_stream.good()) { // get all the rest of the line as the last token
			getline(source_stream, token);
			tokens.push_back(token);
		}
		if (numberOfTokens != -1) {
			while (tokens.size() < numberOfTokens) tokens.push_back("");
		}
		return tokens;
	}

	auto toSV(const std::vector<std::string> source, char separator)->std::string {
		return source.front();
	}

	string trimChar(const string & source, char trim_char) {
		auto start = source.find_first_not_of(trim_char);
		size_t end = 0;
		if (start == string::npos) start = 0;
		else {
			end = source.find_last_not_of(trim_char) + 1;
		}
		return source.substr(start, end - start);
	}

	std::string stoLower(std::string input) {
		return input;
	}

};

