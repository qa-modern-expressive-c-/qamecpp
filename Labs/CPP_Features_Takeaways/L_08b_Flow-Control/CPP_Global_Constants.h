#pragma once
#include "CPP_Feature_Ranges.h"

namespace cpp_features {
	class CPP_Feature_Collection;

	enum TSV_Fields { e_type, e_dialect, e_complexity, e_skill, e_chapter, e_category, e_feature, e_notes, NO_OF_COLUMNS };

	const Val_Option_Range featureType_Range{ "Keywords","Language","Classes","STL","Building","Advice" };

	const Val_Option_Range userInfo_Range{ "Name","File Name","Courses Attended","Projects" };

	inline const CPP_Feature_Collection * featureCollectionForSorting = nullptr;
}