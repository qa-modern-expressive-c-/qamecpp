#pragma once
#include <string>
#include <vector>

namespace  string_utilities
{
	auto splitString(const std::string & source, char separator, size_t numberOfTokens = -1) -> std::vector<std::string>;
	
	auto toSV(const std::vector<std::string>, char separator)->std::string;

	std::string stoLower(std::string input);

	inline auto sizeOfArray = [](auto & array) {return sizeof(array) / sizeof(array[0]); };

	inline auto array_index = [](const auto & array, const auto & text) {
		return find_if(array, array + sizeOfArray(array), [text](auto & array) {return array[0] == text[0]; }) - array;
	};
};

