#pragma once
#include <string>
#include <vector>

namespace  string_utilities
{
	inline auto sizeOfArray = [](auto & array) {return sizeof(array) / sizeof(array[0]); };

	inline auto array_index = [](const auto & array, const auto & text) {
		return find_if(array, array + sizeOfArray(array), [text](auto & array) {return array[0] == text[0]; }) - array;
	};	
	
	std::string stoLower(std::string input);

	auto splitString(const std::string & source, char separator, size_t numberOfTokens = -1) -> std::vector<std::string>;
	
	std::string trimChar(const std::string & source, char trim_char);

	auto toSV(const std::vector<std::string>, char separator) -> std::string;

	inline std::string ctos(char ch) { return std::string(ch != 0, ch); }


};

