#include "pch.h"
#include "CPP_Feature_Ranges.h"
#include "string_utilities.h"
#include <iterator>

namespace cpp_features {
	using namespace std;
	///////////  Val_Option_Range ///////////////////

	auto Val_Option_Range::keyRange() const -> std::vector<std::string> {
		return { begin(), end() };
	}

	auto Val_Option_Range::rangePos(std::string fieldKey) const->std::size_t {
		return distance(begin(), find(begin(), end(), fieldKey));
	}

	auto Val_Option_Range::keyAt(size_t pos) const -> std::string {
		return pos >= size() ? "" : at(pos);
	}

	auto Val_Option_Range::keyMatches(std::string item) const->std::string {
		for (auto & validItem : *this) {
			if (validItem == item) return item;
		}
		return { "" };
	}	
	
	///////////  Key_Val_Option_Range ///////////////////

	Key_Val_Option_Range::Key_Val_Option_Range(initializer_list<string> args) {
		//lambdas
		auto key = [](string str) { 
			for (auto ch : str) if (ch == toupper(ch)) return ch;
			str[0] = toupper(str[0]);
			return str[0];
		};
		
		for (auto & arg : args) {
			push_back({ key(arg), arg });
		}
	}	

	auto Key_Val_Option_Range::keyRange() const -> std::vector<std::string> {
		vector<string> keyRange;
		transform(begin(), end(), back_inserter(keyRange), [](auto pair) { return string{ pair.first }; });
		return keyRange;
	}

	auto Key_Val_Option_Range::rangePos(std::string fieldKey) const->std::size_t {
		auto upperKey = toupper(fieldKey[0]);
		return distance(begin(), find_if(begin(), end(), [upperKey](auto pair) {return pair.first == upperKey; }));
	}

	auto Key_Val_Option_Range::keyAt(size_t pos) const -> std::string {
		return "";
	}

	auto Key_Val_Option_Range::keyMatches(std::string item) const->std::string {
		//for (auto & validItem : *this) {
		//	if (validItem == item) return item;
		//}
		return { "" };
	}

	auto createSequentialRange(string prefix, size_t noOfElements) -> Val_Option_Range {
		// lambda
		auto elementString = [prefix, elementCount = 0]() mutable {
			auto element = ostringstream{};
			element << prefix << setw(2) << setfill('0') << ++elementCount;
			return element.str();
		};

		// algorithm
		auto sequentialRange = Val_Option_Range(noOfElements);
		generate(sequentialRange.begin(), sequentialRange.end(), elementString);
		return sequentialRange;
	}
}