#pragma once
#include "CPP_Global_Constants.h"
#include <string>
#include <vector>

/////////////// Feature Classes /////////////
namespace cpp_features {

	class CPP_Language_Feature
	{
	public:
		CPP_Language_Feature(std::vector<std::string> args);

		// Queries
		auto asVector() const -> std::vector<std::string>;
		std::string aspect(TSV_Fields) const;

		// Modifiers
		void set_type(std::string type) { _type = type; }
		void set_dialect(std::string dialect) { _dialect = dialect; }
		void set_skill(std::string skill) { _skill = skill; }
		void set_chapter(std::string chapter) { _chapter = chapter; }
		void set_complexity(std::string complexity) { _complexity = complexity; }
		void set_category(std::string category) { _category = category; }
		void set_feature(std::string feature) { _feature = feature; }
		void set_notes(std::string notes) { _notes = notes; }

	private:
		std::string _type;
		std::string _dialect;
		std::string _complexity;
		std::string _skill;
		std::string _chapter;
		std::string _category;
		std::string _feature;
		std::string _notes;
	};
}