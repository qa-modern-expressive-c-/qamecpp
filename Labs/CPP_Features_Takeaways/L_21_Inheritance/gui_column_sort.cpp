#include "pch.h"
#include "gui_column_sort.h"
#include "CPP_All_Features_Collection.h"
#include "string_utilities.h"

using namespace std;
using namespace cpp_features;
using namespace string_utilities;
// global column sorting functions

bool compareType(const string & text_l, nana::any *, const string & text_r, nana::any *, bool reverse)
{
	auto toInt = [](const string & text) {
		return featureType_Range.rangePos(text);
	};

	return (reverse ? toInt(text_l) > toInt(text_r) : toInt(text_l) < toInt(text_r));
}

bool compareLevel(const string & text_l, nana::any *, const string & text_r, nana::any *, bool reverse)
{
	auto toInt = [](const string & text) {
		return featureCollectionForSorting->optionRangeFor(e_complexity).rangePos(text);
	};

	return (reverse ? toInt(text_l) > toInt(text_r) : toInt(text_l) < toInt(text_r));
}

bool compareCategory(const string & text_l, nana::any *, const string & text_r, nana::any *, bool reverse)
{
	return (reverse ? text_l > text_r : text_l < text_r);
}

bool compareSkill(const string & text_l, nana::any *, const string & text_r, nana::any *, bool reverse)
{
	auto toInt = [](const string & text) {
		return featureCollectionForSorting->optionRangeFor(e_skill).rangePos(text);
	};

	return (reverse ? toInt(text_l) > toInt(text_r) : toInt(text_l) < toInt(text_r));
}

bool compareChapter(const string & text_l, nana::any *, const string & text_r, nana::any *, bool reverse)
{
	auto toInt = [](const string & text) {
		return featureCollectionForSorting->optionRangeFor(e_chapter).rangePos(text);
	};

	return (reverse ? toInt(text_l) > toInt(text_r) : toInt(text_l) < toInt(text_r));
}

bool compareFeature(const string & text_l, nana::any *, const string & text_r, nana::any *, bool reverse)
{
	return (reverse ? stoLower(text_l) > stoLower(text_r) : stoLower(text_l) < stoLower(text_r));
}