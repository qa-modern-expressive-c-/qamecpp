#pragma once
#include "string_utilities.h"
#include <string>
#include <deque>
#include <initializer_list>

namespace cpp_features {

	class I_Option_Range {
	public:
		// Queries
		auto keyRange() const->std::vector<std::string>;
		auto valueRange() const->std::vector<std::string>;
		auto keyAt(size_t pos) const->std::string;
		auto rangePos(std::string fieldKey) const->std::size_t;
		auto keyMatches(std::string item) const->std::string;
	};

	class Val_Option_Range : public I_Option_Range, public std::deque<std::string> {
	public:
		using std::deque<std::string>::deque;
		// Queries
		auto keyRange() const->std::vector<std::string>;
		auto valueRange() const -> std::vector<std::string> { return keyRange(); }
		auto keyAt(size_t pos) const->std::string;
		auto rangePos(std::string fieldKey) const -> std::size_t;
		auto keyMatches(std::string item) const->std::string;
	};

	class Key_Val_Option_Range : public I_Option_Range, public std::deque<std::pair<char, std::string>> {
	public:
		using std::deque<std::pair<char, std::string>>::deque;
		Key_Val_Option_Range(std::initializer_list<std::string>);
		// Queries
		auto keyRange() const->std::vector<std::string>;
		auto valueRange() const->std::vector<std::string> { return keyRange(); };
		auto keyAt(size_t pos) const->std::string;
		auto rangePos(std::string fieldKey) const -> std::size_t;
		auto keyMatches(std::string item) const->std::string;
	};

	auto createSequentialRange(std::string prefix, size_t noOfElements) -> Val_Option_Range;
}