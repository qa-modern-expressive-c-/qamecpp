#pragma once
#include "CPP_Feature_Collection.h"
#include <vector>
#include <string>

namespace cpp_features {
	constexpr char ALL_FEATURES[] = "";

	class CPP_All_Features_Collections {
	public:
		// Queries
		auto featureSet(std::string featureTypeKey = ALL_FEATURES) const -> const CPP_Feature_Collection &;
		auto features(std::string featureTypeKey) const->std::vector<std::vector<std::string>>;

		// Modifiers
		auto featureSet(std::string featureTypeKey = ALL_FEATURES)->CPP_Feature_Collection &;
		void tidy_features();
		void assignFeature(std::string tsv_line);
		void editFeature(std::vector<std::string> original, std::vector<std::string> edit);
		void eraseFeature(std::vector<std::string> original);
		void appendFeature(std::vector<std::string> newFeature);
		void clear();

	private:
		CPP_Feature_Collection _language_features;
	};
}