#include "pch.h"
#include "gui.h"	// user-defined gui definition
// nana GUI Creation Headers...
#include <nana/gui/wvl.hpp> // for nana::exec()
// STL Headers...
#include <filesystem>
//#include <iostream>
namespace fs = std::filesystem;

int main()
{
	//std::cout << fs::current_path() << std::endl;
	GUI gui{ fs::current_path().parent_path().parent_path().parent_path() /= "Delegate Projects" };
	gui.show();

	nana::exec();
}