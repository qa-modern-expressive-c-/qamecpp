#include "pch.h"
#include "userInfo.h"
#include "string_utilities.h"

using namespace std;
using namespace  string_utilities;
using namespace cpp_features;

namespace user_info {

	UserInfo::UserInfo(const std::filesystem::path & rootPath, std::filesystem::path extension)
		: _fileHandler(rootPath, extension)
	{
	}

	void UserInfo::saveAllFeatures() const {
	}

	void UserInfo::load_FeaturesFile(size_t fileSelectionIndex) {
		allFeatures().clear();
		auto fileLines = files().getLinesFromFileAt(fileSelectionIndex);
		for (auto line : fileLines) allFeatures().assignFeature(line);
	}

	std::vector<std::string> UserInfo::getField(E_UserInfo field) const {
		switch (field) {
		case e_userName: return { _userName };
		case e_featuresFileName: return { _CPP_Features_filename };
		case e_cpp_courses: return _CPP_Courses;
		case e_projects: return _projects;
		default: return { "" };
		}
	}

	void UserInfo::setField(E_UserInfo field, std::vector<std::string> fields) {
		switch (field) {
		case e_userName: _userName = fields[0]; break;
		case e_featuresFileName: _CPP_Features_filename = fields[0]; break;
		case e_cpp_courses: _CPP_Courses = fields; break;
		case e_projects: _projects = fields; break;
		default:;
		}
	}

	void UserInfo::saveInfoFile() const {
		auto lines = getField(e_userName);
		if (lines[e_userName] != "") {
			lines.push_back(getField(e_featuresFileName)[0]);
			lines.push_back(toSV(getField(e_cpp_courses), '\t'));
			lines.push_back(toSV(getField(e_projects), '\t'));
			_fileHandler.writeFile(_userName + string("_") + USER_FILE_POSTSCRIPT, lines);
		}
	}

	void UserInfo::load_InfoFile(size_t fileSelectionIndex) {
		auto lines = _fileHandler.getLinesFromFileAt(fileSelectionIndex);
		setField(e_userName, { lines[e_userName] });
		setField(e_featuresFileName, { lines[e_featuresFileName] });
		setField(e_cpp_courses, splitString(lines[e_cpp_courses], '\t'));
		setField(e_projects, splitString(lines[e_projects], '\t'));
	}
}