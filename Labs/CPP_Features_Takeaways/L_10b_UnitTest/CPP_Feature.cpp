#include "pch.h"
#include "CPP_Feature.h"
#include "string_utilities.h"
#include <iostream>

namespace cpp_features {
	using namespace string_utilities;
	using namespace std;

	//////////////// CPP_Language_Feature //////////////////////////

	CPP_Language_Feature::CPP_Language_Feature(std::vector<std::string> args) {
		set_type(args.at(e_type));
		set_dialect(args.at(e_dialect));
		set_skill(args.at(e_skill));
		set_chapter(args.at(e_chapter));
		set_complexity(args.at(e_complexity));
		set_category(args.at(e_category));
		set_feature(args.at(e_feature));
		set_notes(args.at(e_notes));
	}

	std::string CPP_Language_Feature::aspect(TSV_Fields field) const {
		switch (field) {
		case e_type:		return _type;
		case e_dialect:		return _dialect;
		case e_complexity:	return _complexity;
		case e_skill:		return _skill;
		case e_chapter:			return _chapter;
		case e_category:	return _category;
		case e_feature:		return _feature;
		case e_notes:		return _notes;
		default: return "";
		}
	}

	auto CPP_Language_Feature::asVector() const -> vector<string> {
		return { aspect(e_type), aspect(e_dialect), aspect(e_complexity), aspect(e_skill), aspect(e_chapter), aspect(e_category), aspect(e_feature), aspect(e_notes) };
	}
}