#pragma once
#include "fileHandler.h"
#include "CPP_All_Features_Collection.h"
#include "string_utilities.h"
#include <filesystem>
#include <string>
#include <vector>

namespace user_info {
	constexpr char USER_FILE_POSTSCRIPT[] = "UserInfo";
	constexpr char user_info_labels[][20] = { "Name","File Name","Courses Attended","Projects" };
	constexpr auto sizeOf_user_info_labels = string_utilities::sizeOfArray(user_info_labels);
	enum E_UserInfo { e_userName, e_featuresFileName, e_cpp_courses, e_projects, e_NO_OF_USER_FIELDS };

	class UserInfo
	{
	public:
		UserInfo(const std::filesystem::path & rootPath, std::filesystem::path extension);
		// Queries
		auto getField(E_UserInfo field) const -> std::vector<std::string>;
		void saveInfoFile() const;
		void saveAllFeatures() const;
		FileHandler & files() const { return _fileHandler; }
		auto const & allFeatures() const { return _all_features; }

		// Modifiers
		void setField(E_UserInfo field, std::vector<std::string>);
		void load_InfoFile(size_t fileSelectionIndex);
		void load_FeaturesFile(size_t fileSelectionIndex);
		auto & allFeatures() { return _all_features; }
	private:
		std::string _userName;
		std::string _CPP_Features_filename;
		std::vector<std::string> _CPP_Courses;
		std::vector<std::string> _projects;
		mutable FileHandler _fileHandler;
		cpp_features::CPP_All_Features_Collections _all_features;
	};
}


