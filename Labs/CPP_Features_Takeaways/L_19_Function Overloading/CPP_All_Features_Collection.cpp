#include "pch.h"
#include "CPP_All_Features_Collection.h"
#include "string_utilities.h"

namespace cpp_features {
	using namespace string_utilities;
	using namespace std;

	vector<vector<string>> CPP_All_Features_Collections::features(string featureTypeKey) const {
		vector<vector<string>> featureList;
		// Lambda
		auto appendFeatureSet = [&featureList, this](auto featureType) {
			auto & feature_set = featureSet(featureType);
			for (auto language_feature : feature_set) {
				auto feature = language_feature.asVector();
				featureList.push_back(feature);
			}
		};

		// algorithm
		if (featureTypeKey == ALL_FEATURES) {
			for (auto featureKey : featureType_Range.keyRange()) {
				appendFeatureSet(featureKey);
			}
		}
		else {
			appendFeatureSet(featureTypeKey);
		}

		return featureList;
	}

	void CPP_All_Features_Collections::assignFeature(std::string tsv_line) {
		auto splitLine = splitString(tsv_line, '\t', NO_OF_COLUMNS);
		auto type_key = splitLine.at(e_type);
		if (!type_key.empty()) {
			featureSet(type_key).addFeature(splitLine);
		}
	}
	   
	void CPP_All_Features_Collections::clear() {
		for (auto featureKey : featureType_Range.keyRange()) {
			auto & feature_set = featureSet(featureKey);
			feature_set.clear();
		}
	}

	void CPP_All_Features_Collections::tidy_features() {
		// lambdas
		auto setMissingDialect = [](auto & feature) {
			if (feature[e_dialect].empty()) {
				feature[e_dialect] = "03";
			}
		};

		auto trimQuotes = [](auto & feature) {
			feature[e_feature] = trimChar(feature[e_feature], '"');
			feature[e_notes] = trimChar(feature[e_notes], '"');
		};

		auto doTidy = [setMissingDialect, trimQuotes](auto feature) {
			setMissingDialect(feature);
			trimQuotes(feature);
			return feature;
		};

		// algorithm
		for (auto featureKey : featureType_Range.keyRange()) {
			auto & feature_set = featureSet(featureKey);
			for (auto it = feature_set.begin(); it != feature_set.end(); ++it) {
				feature_set.editFeature(distance(feature_set.begin(), it), doTidy(it->asVector()));
			}
		}
	}

	void CPP_All_Features_Collections::appendFeature(std::vector<std::string> newFeature) {
		auto & feature_set = featureSet(newFeature[e_type]);
		feature_set.addFeature(newFeature);
	}

	void CPP_All_Features_Collections::eraseFeature(std::vector<std::string> original) {
		auto & feature_set = featureSet(original[e_type]);
		auto foundIT = feature_set.findFeature(original);
		feature_set.eraseFeature(distance(feature_set.begin(), foundIT));
	}

	void CPP_All_Features_Collections::editFeature(vector<string> original, vector<string> edit) {
		auto feature_set = &featureSet(original[e_type]); // needs to be re-assignable, so can't use a reference
		auto foundIT = feature_set->findFeature(original);
		auto foundPos = distance(feature_set->begin(), foundIT);
		if (foundIT != feature_set->end()) {
			if (original[e_type] != edit[e_type]) {
				feature_set->eraseFeature(foundPos);
				feature_set = &featureSet(edit[e_type]);
				feature_set->addFeature(edit);
			}
			else {
				feature_set->editFeature(foundPos, edit);
			}
		}
	}

	CPP_Feature_Collection & CPP_All_Features_Collections::featureSet(std::string featureTypeKey) {
		switch (featureTypeKey[0]) {
		case 'K': return _keyword_features;
		case 'L': return _language_features;
		case 'C': return _class_features;
		case 'S': return _STL_features;
		case 'B': return _build_features;
		case 'A': return _advice_features;
		default: return _keyword_features;
		}
	}
}
