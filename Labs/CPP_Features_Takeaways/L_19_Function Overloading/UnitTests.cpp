#include "pch.h"
#include <catch.hpp>
#include "string_utilities.h"
#include <string>
#include "CPP_Feature_Ranges.h"

using namespace std;
using namespace string_utilities;

// Lab 10 Unit Tests

TEST_CASE("Each Column Present", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("First Column Missing", "[SplitString]") {
	auto sourceString = string{ ",b,c,d" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("Last Column Missing", "[SplitString]") {
	auto sourceString = string{ "a,b,c" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("More Columns than Expected", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d,e" };
	auto tokenized = splitString(sourceString, ',', 4);
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d,e");
	CHECK(tokenized.size() == 4);
}

TEST_CASE("Unlimited Columns", "[SplitString]") {
	auto sourceString = string{ "a,b,c,d,e,f,g" };
	auto tokenized = splitString(sourceString, ',');
	CHECK(tokenized[0] == "a");
	CHECK(tokenized[1] == "b");
	CHECK(tokenized[2] == "c");
	CHECK(tokenized[3] == "d");
	CHECK(tokenized[4] == "e");
	CHECK(tokenized[5] == "f");
	CHECK(tokenized[6] == "g");
	CHECK(tokenized.size() == 7);
}

// Lab 14 Lambdas

TEST_CASE("No trims", "[TrimChar]") {
	auto test = string{ "test" };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Front trim", "[TrimChar]") {
	auto test = string{ "  test" };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Back trim", "[TrimChar]") {
	auto test = string{ "test  " };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Both-end trim", "[TrimChar]") {
	auto test = string{ "  test  " };
	CHECK(trimChar(test, ' ') == "test");
}

TEST_CASE("Trim all chars away", "[TrimChar]") {
	auto test = string{ "   " };
	CHECK(trimChar(test, ' ') == "");
}

TEST_CASE("Trim Empty Str", "[TrimChar]") {
	auto test = string{ "" };
	CHECK(trimChar(test, ' ') == "");
}

// Lab 15 Algorithms

TEST_CASE("CSV", "[toSV]") {
	auto testVector = vector<string>{ "S1","S2","S3" };
	CHECK(toSV(testVector, ',') == "S1,S2,S3");
}

TEST_CASE("emptySV", "[toSV]") {
	auto testVector = vector<string>{ };
	CHECK(toSV(testVector, ',') == "");
}

TEST_CASE("lowerStr", "[stoLower]") {
	auto test = string{ "Fred Bloggs" };
	CHECK(stoLower(test) == "fred bloggs");
}

// Lab 17 Container Types

TEST_CASE("rangePos-V", "[Val_Option_Range]") {
	using namespace cpp_features;
	auto test_Range = Val_Option_Range{ "03","11","14" };
	CHECK(test_Range.rangePos("11") == 1);
	CHECK(test_Range.rangePos("1") == 3);
}

// Lab 18 Type Conversions

TEST_CASE("chapterRange", "[createChapterRange]") {
	using namespace cpp_features;
	auto chapterRange = createSequentialRange("E", 10);
	CHECK(chapterRange.at(0) == "E01");
	CHECK(chapterRange.at(8) == "E09");
	CHECK(chapterRange.at(9) == "E10");
	CHECK(chapterRange.size() == 10);
}

// Lab 19 Function Overloading

TEST_CASE("ketAt", "[Val_Option_Range]") {
	using namespace cpp_features;
	auto kv = Val_Option_Range{ "Abc","Cde","Fgh" };
	CHECK(kv.keyAt(0) == "Abc");
	CHECK(kv.keyAt(1) == "Cde");
	CHECK(kv.keyAt(5) == "");
}