#include "pch.h"
#include "CPP_Feature_Ranges.h"
#include "string_utilities.h"

namespace cpp_features {
	using namespace std;
	///////////  Val_Option_Range ///////////////////

	auto Val_Option_Range::keyRange() const -> std::vector<std::string> {
		return { begin(), end() };
	}

	auto Val_Option_Range::rangePos(std::string fieldKey) const->std::size_t {
		return distance(begin(), find(begin(), end(), fieldKey));
	}

	auto Val_Option_Range::keyAt(size_t pos) const -> std::string {
		return "";
	}

	auto Val_Option_Range::keyMatches(std::string item) const->std::string {
		for (auto & validItem : *this) {
			if (validItem == item) return item;
		}
		return { "" };
	}

	auto createSequentialRange(string prefix, size_t noOfElements) -> Val_Option_Range {
		// lambda
		auto elementString = [prefix, elementCount = 0]() mutable {
			auto element = ostringstream{};
			element << prefix << setw(2) << setfill('0') << ++elementCount;
			return element.str();
		};

		// algorithm
		auto sequentialRange = Val_Option_Range(noOfElements);
		generate(sequentialRange.begin(), sequentialRange.end(), elementString);
		return sequentialRange;
	}
}


