#include "pch.h"
#include "CPP_Feature_Collection.h"


namespace cpp_features {
	using namespace std;

	namespace {
		Val_Option_Range dialect_Range{ "03","11","14","17","20", "23" };
		Val_Option_Range complexity_Range{ "Basic","Intermediate","Advanced","eXpert" };
		Val_Option_Range skills_Range{ "Zero", "Aware", "Basic","Competent" };
		Val_Option_Range chapters_Range = createSequentialRange("E", 23); // { "E01", "E02", "E03", "E04", "E05" };
		Val_Option_Range language_categories_Range{
			// Keyword, Language & Class categories
			"Declaration"
			, "Initialisation"
			, "Operations"
			, "Behaviour"
			, "Flow Control"
			, "UDT"
			, "Conversion"
			, "Scope"
			, "Lifetimes"
			, "Literal"
			, "Templates"
			, "Errors"
			// STL Categories
			, "Container"
			, "Algorithm"
			, "Generics"
			, "Utility"
			, "Memory"
			, "Types/Information"
			, "Multi-Threading"
			, "Date/Time/Locale"
			, "Input/Output"
			, "Numerics"
			, "Strings"
			// Build Categories
			, "Messages"
			, "Dependancies"
			, "Attributes"
			// Advice
			, "Idiom"
			, "Coding Guidline"
			, "Design Pattern"
			, "Design Principle"
		};

		Val_Option_Range null_Range{};
	}

	/////////////// CPP_Feature_Collection ///////////////////

	auto CPP_Feature_Collection::optionRangeFor(TSV_Fields field) -> const Val_Option_Range & {
		switch (field) {
		case e_dialect:		return dialect_Range;
		case e_complexity:	return complexity_Range;
		case e_skill:		return skills_Range;
		case e_chapter:		return chapters_Range;
		case e_category:	return language_categories_Range;
		default: return null_Range;
		}
	}

	auto CPP_Feature_Collection::keyRange(TSV_Fields field) const -> vector<string> {
		return optionRangeFor(field).keyRange();
	}

	auto CPP_Feature_Collection::valueRange(TSV_Fields field) const -> vector<string> {
		return optionRangeFor(field).valueRange();
	}

	auto CPP_Feature_Collection::rangePos(TSV_Fields field, string fieldKey) const -> size_t {
		return optionRangeFor(field).rangePos( fieldKey);
	}

	auto CPP_Feature_Collection::keyAt(TSV_Fields field, size_t pos) const -> string {
		return optionRangeFor(field).keyAt(pos);
	}

	auto CPP_Feature_Collection::findFeature(std::vector<std::string> feature) -> std::vector<CPP_Language_Feature>::iterator {
		return find_if(begin(), end(), [feature](auto languageFeature) { return languageFeature.asVector() == feature; });
	}

	void CPP_Feature_Collection::addFeature(std::vector<std::string> arg) {
		push_back(CPP_Language_Feature{ arg });
	}

	void CPP_Feature_Collection::editFeature(size_t pos, std::vector<std::string> edit) {
		at(pos) = CPP_Language_Feature{ edit };
	}

	void CPP_Feature_Collection::eraseFeature(size_t pos) {
		erase(next(vector::begin(), pos));
	}
}
