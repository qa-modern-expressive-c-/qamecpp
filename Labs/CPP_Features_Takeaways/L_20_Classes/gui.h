#pragma once
#include "userInfo.h"
// nana GUI Creation Headers...
#include <nana/gui/wvl.hpp> 
#include <nana/gui/place.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/listbox.hpp>
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/widgets/combox.hpp>
// STL Headers...
#include <filesystem>

class GUI : public nana::form
{
	public:
		GUI(const std::filesystem::path);

		std::vector<std::string> getFeature(nana::listbox::index_pair pos) const;
		std::vector<std::string> getFeatureFromEdit() const;

		void setFeature(nana::listbox::index_pair pos, std::vector<std::string>);
		void appendFeature(std::vector<std::string>);
		void eraseFeature();

	private:
		// initialise
		void _initialise_buttons();
		void _initialise_folderPath_Element();
		void _initialise_Combos();
		void _initialise_fileList_Element();
		void _initialise_UserList_Element();
		void _initialise_featureList_Element();
		void _initialise_editBox_Elements();

		// event handlers
		void _getFolderPath();
		void _load_File();
		void _tidy_FeatureList();
		void _save_File();
		//void _process_edit();
		//void _insertNewFeature();
		void _fileSelected(const nana::arg_listbox & ar_lbx);
		void _catchUser();
		void _catchFeature();
		void _type_combo_changed();
		void _feature_combo_changed();
		void _load_Feature_list();
		void _search();
		void _nextMatch(int direction, bool reset = false);
		//void _featureClick();
		
		// helpers
		void _copyToEditBox(nana::listbox::index_pair pos);
		void _load_FileList();
		bool _save_user_info();
		void _saveEdit();
		void _saveNew();
		void _clearFeaturesSelection();
		void _setFeaturesSelection(nana::listbox::index_pair pos, bool showThis);

		// GUI elements
		nana::label   _folder_lbl;
		nana::textbox _folderPath;
		nana::listbox _fileList;
		nana::listbox _userList;
		nana::button _loadFileCmd;
		nana::button _tidyFileCmd;
		nana::button _saveAsCmd;
		nana::button _saveEditCmd;
		nana::button _newCmd;
		nana::button _delCmd;
		nana::button _prevCmd;
		nana::button _nextCmd;
		nana::combox _typeCmbo;
		nana::combox _dialectCmbo;
		nana::combox _levelCmbo;
		nana::combox _skillCmbo;
		nana::combox _chapterCmbo;
		nana::combox _categoryCmbo;
		nana::label _searchLbl;
		nana::textbox _searchBox;
		nana::listbox _featureList;
		nana::combox _filter;
		nana::textbox _editTxt;
		nana::textbox _notesTxt;
		nana::event_handle _featureSelectEvent;
		// GUI Layout
		void layout();
		// Objects for State. C++11 nested classes get private access
		class Change_Behaviour;
		class I_Behaviour;
		class Select_Behaviour;
		class Search_Behaviour;
		class EditFeature_Behaviour;
		class NewFeature_Behaviour;
		class ViewUserInfo_Behaviour;
		class EditUserInfo_Behaviour;
		I_Behaviour * _behaviour = nullptr;
		void _changeState(I_Behaviour * newBehaviour);

		// Supporting objects
		user_info::UserInfo _user;
		nana::place _place;
		static Change_Behaviour _change_behaviour;
};

class GUI::I_Behaviour {
public:
	virtual void setGUI_state(GUI & gui) const = 0;
	virtual void save_cmd(GUI & gui) const {}
	virtual void new_cmd(GUI & gui) const {}
	virtual void del_cmd(GUI & gui) const {}
	virtual void itemEdited(GUI & gui) const;
	void setDel_enable(GUI & gui) const;
protected:
private:
};

class GUI::Select_Behaviour : public GUI::I_Behaviour {
public:
	void setGUI_state(GUI & gui) const override;
	void new_cmd(GUI & gui) const override;
	void del_cmd(GUI & gui) const override;
};

class GUI::Search_Behaviour : public I_Behaviour {
public:
	void setGUI_state(GUI & gui) const override;
	void new_cmd(GUI & gui) const override;
	void del_cmd(GUI & gui) const override;
};

class GUI::EditFeature_Behaviour : public I_Behaviour {
public:
	void setGUI_state(GUI & gui) const override;
	void save_cmd(GUI & gui) const override;
	void new_cmd(GUI & gui) const override;
	void del_cmd(GUI & gui) const override; // cancel
};

class GUI::NewFeature_Behaviour : public I_Behaviour {
public:
	void setGUI_state(GUI & gui) const override;
	void save_cmd(GUI & gui) const override;
	void del_cmd(GUI & gui) const override; // cancel
};

class GUI::ViewUserInfo_Behaviour : public I_Behaviour {
public:
	void setGUI_state(GUI & gui) const override;
	void itemEdited(GUI & gui) const override;
};

class GUI::EditUserInfo_Behaviour : public I_Behaviour {
public:
	void setGUI_state(GUI & gui) const override;
	void save_cmd(GUI & gui) const override;
	void del_cmd(GUI & gui) const override; // cancel
	void itemEdited(GUI & gui) const override {};
};

class GUI::Change_Behaviour {
public:
	enum class Transitions : int;
	void transitionTo(Transitions transition) const;
	void setGui(GUI & gui);

private:
	inline static GUI::Select_Behaviour _select;
	inline static GUI::Search_Behaviour _search;
	inline static GUI::EditFeature_Behaviour _editFeature;
	inline static GUI::NewFeature_Behaviour _newFeature;
	inline static GUI::ViewUserInfo_Behaviour _viewUser;
	inline static GUI::EditUserInfo_Behaviour _editUser;
	GUI * _gui = nullptr;
};
